Site dos dados públicos do trabalho que propôs o Rastro-DM

Contato: borela@tcu.gov.br

Link para o trabalho: https://portal.tcu.gov.br/biblioteca-digital/mineracao-de-dados-com-rastro-boas-praticas-para-documentacao-de-processo-e-sua-aplicacao-em-um-projeto-de-classificacao-textual.htm

Link para apresentação no 5o Seminário Internacional de Análise de Dados (2018): 
    Rastro-DM: https://youtu.be/9iqnCyqy1gQ?t=2395
    Projeto Cladop: https://youtu.be/q096bJ07II4?t=2139 

Referência:
CASTRO, Marcus Vinícius Borela. Mineração de Dados com Rastro: Boas Práticas para Documentação de Processos e sua Aplicação em um Projeto de Classificação Textual. 2019. 

Para citação:

Castro, Marcus. Balaniuk, Remis. Rastro-DM: Mineração de Dados com Rastro. Revista do TCU. edição n. 145. 2020. Disponível em: https://portal.tcu.gov.br/lumis/portal/file/fileDownload.jsp?fileId=8A81881F79E7AFBA017A58B55DC15C71. Acesso em 21 jul.2022.


Trabalho de Conclusão de Curso (Especialização em Análise de Dados para o Controle) – Escola Superior do Tribunal de Contas da União, Instituto Serzedello Corrêa, Brasília DF. 

Este trabalho propõe um conjunto de boas práticas de apoio a projetos de mineração de dados (DM), Rastro-DM, com foco na documentação do processo (e não do produto) e de seus conceitos basilares: as Definições de Ação, os Treinamentos realizados e os Aprendizados concebidos. As práticas propostas são complementares às metodologias estruturantes de DM, tal como o CRISP-DM, que trazem todo o arcabouço metodológico e paradigmático para o processo de DM. Ilustra-se o seu uso em um projeto de classificação textual de documentos em PDF associados a danos ao Erário Público Federal Brasileiro denominado Cladop. Mostra-se, no contexto do Cladop, o uso do rastro documental para a geração semi-automática de relatórios e a sua integração com uma rotina de monitoramento automático proposta para classificadores em produção. A construção do rastro DM em um projeto é um passo fundamental em direção a um potencial salto organizacional, a ser obtido com a partilha e o uso do rastro de forma corporativa.


ABSTRACT
Data Mining with Trail: Best Practices for Process Documentation and its Application in a Textual Classification Project

This paper proposes a set of best practices to support data mining (DM) projects, Rastro-DM, focusing on the documentation of some basic concepts: the Definition of Actions, the Trainings performed and the Learning conceived. The proposed practices are complementary to the structuring methodologies of DM, such as the CRISP-DM, which estabilish a methodological and paradigmatic framework for the DM process. The application of best practices is illustrated in a project called Cladop that was created for the classification of PDF documents associated with the investigatory process of damages to the Brazilian Federal Public Treasury. In the context of Cladop, two benefits are shown: the use of the document trail for semi-automatic report generation and its integration with an automatic monitoring procedure proposed for classifiers in production. Building the DM trail in the context of a project is a fundamental step towards a potential organizational-wide benefits to be achieved by sharing and using the trail across the enterprise.

Keywords: Data mining. Data analysis. Data science. Machine learning. Organizational knowledge. Methodology. Best Practices.  Data analysis in Government. Textual classification. Documentation. Documentation of data mining projects. Automatic Reporting. Monitoring procedure for classifiers.

