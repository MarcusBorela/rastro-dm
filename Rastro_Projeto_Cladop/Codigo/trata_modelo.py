import pickle
import numpy as np
import datetime
from unicodedata import normalize
from sklearn.decomposition import TruncatedSVD  # usado ao gerar lista

# Author: marcusborela@yahoo.com.br
# originado de parte do antigo arquivo util.py    se necessário  busca de código antigo

def adiciona_colunas_extras(X,parm_colunas_extras,df):
    vcolunas_extras_texto=','.join(parm_colunas_extras).strip()
    if vcolunas_extras_texto != '':  # tem colunas
        for coluna in parm_colunas_extras:
            nome_coluna = _pega_ultima_palavra(coluna)  # para tratar caso de alias
            X = np.column_stack((X, df[nome_coluna.upper()]))
        # aplicar log
    return X

def _pega_ultima_palavra(pstring):
    return pstring.rsplit(None, 1)[-1]

def carrega_modelo_palavras(parm_metodo_extracao_txt,parm_bag_num_palavras):
    """
    Recebe parâmetros 
    E usa modelos de palavras (Tokenizer) pré-carregados
    Considera:
      bag_num_palavras, metodo_extracao_txt, bag_ind_modo_numerico

    gerado pelo Gera_modelo_palavras.ipynb
    """
    global mod_palavra_conteudo
    if parm_metodo_extracao_txt not in mod_palavra_conteudo:
        mod_palavra_conteudo[parm_metodo_extracao_txt] = pickle.load(open('modelo/modelo_palavras_metodo_'+str(parm_metodo_extracao_txt)+'.pkl',"rb"))
        mod_palavra_conteudo[parm_metodo_extracao_txt].num_words=parm_bag_num_palavras
        print('Modelo Bag Conteudo carregado - qtd de palavra: ', len(mod_palavra_conteudo[parm_metodo_extracao_txt].word_index), 'qtd de documentos: ', mod_palavra_conteudo[parm_metodo_extracao_txt].document_count)
    else:        
        print('Já estava carregado modelo Bag Conteudo - qtd de palavra: ', len(mod_palavra_conteudo[parm_metodo_extracao_txt].word_index), 'qtd de documentos: ', mod_palavra_conteudo[parm_metodo_extracao_txt].document_count)
        if mod_palavra_conteudo[parm_metodo_extracao_txt].num_words != parm_bag_num_palavras:
            mod_palavra_nome_arquivo[parm_metodo_extracao_txt].num_words=parm_bag_num_palavras

def carrega_modelo_palavras_nome_arquivo(parm_bag_num_palavras, parm_metodo_extracao_nome):
    global mod_palavra_nome_arquivo
    assert parm_metodo_extracao_nome in (1,2), "Não programada versão que não seja 1 para extração de nome de arquivo"
    if parm_metodo_extracao_nome not in mod_palavra_nome_arquivo:
        mod_palavra_nome_arquivo[parm_metodo_extracao_nome] = pickle.load(open('modelo/modelo_nome_arquivo_metodo_v'+str(parm_metodo_extracao_nome)+'.pkl',"rb"))
        mod_palavra_nome_arquivo[parm_metodo_extracao_nome].num_words=parm_bag_num_palavras
        print('Carregado modelo Bag nome arquivo '+str(parm_metodo_extracao_nome) + ' - qtd de palavra: ', len(mod_palavra_nome_arquivo[parm_metodo_extracao_nome].word_index), 'qtd de documentos: ', mod_palavra_nome_arquivo[parm_metodo_extracao_nome].document_count)
    else:        
        print('Já estava carregado modelo Bag nome arquivo '+ str(parm_metodo_extracao_nome) + ' - qtd de palavra: ', len(mod_palavra_nome_arquivo[parm_metodo_extracao_nome].word_index), 'qtd de documentos: ', mod_palavra_nome_arquivo[parm_metodo_extracao_nome].document_count)
        if mod_palavra_nome_arquivo[parm_metodo_extracao_nome].num_words != parm_bag_num_palavras:
            mod_palavra_nome_arquivo[parm_metodo_extracao_nome].num_words=parm_bag_num_palavras


def trata_reducao_dimensao(X, parm_redutor, parm_tamanho):
    global mod_redutor_tsvd_768
    if parm_redutor != '' and parm_tamanho != '':
        # and "n_components=768" in str(parm_redutor)
        if  "TruncatedSVD" in str(parm_redutor) \
        and parm_tamanho == 768 \
        and "arpack" in str(parm_redutor) \
        and  "n_iter=5" in str(parm_redutor) \
        and  "random_state=42" in str(parm_redutor) \
        and "tol=0.0" in str(parm_redutor): 
            carrega_modelo_redutor_tsvd_768()
            return mod_redutor_tsvd_768.transform(X)
        else:
            parm_redutor.set_params(n_components=parm_tamanho)
            parm_redutor.fit(X)
            return parm_redutor.transform(X), parm_redutor
    else:
        #print('Não foi passada informação para redução de dimensões')
        return X, parm_redutor

def carrega_modelo_redutor_tsvd_768():
    global mod_redutor_tsvd_768
    if mod_redutor_tsvd_768 is None:
        mod_redutor_tsvd_768 = pickle.load(open('modelo/modelo_reducao_truncatedSVD_768.pkl',"rb"))
        print('Carregado modelo de redução de palavras truncatedSVD_768: ', mod_redutor_tsvd_768.n_components)



def traduz_X_por_bag(parm_array, parm_metodo_extracao_txt, parm_bag_num_palavras, parm_bag_ind_modo_numerico):
    # Converte palavras em números nos textos a partir do modelo de palavras
    global mod_palavra_conteudo
    carrega_modelo_palavras(parm_metodo_extracao_txt,parm_bag_num_palavras)
    X = mod_palavra_conteudo[parm_metodo_extracao_txt].texts_to_matrix(parm_array, mode=parm_bag_ind_modo_numerico)
    return X


def traduz_nome_arquivo(X,df,parm_bag_num_palavras_nome_arquivo,parm_bag_ind_modo_numerico,parm_metodo_extracao_nome):
    """
    Adiciona bag do nome do arquivo em X
        Converte palavras do nome do arquivo em números nos textos a partir do modelo de palavras
    Exclui coluna nome_arquivo em X
    """
    assert parm_metodo_extracao_nome in [1,2], "Não programada versão de extração de nome de arquivo. Somente 1 e 2"  
    global mod_palavra_nome_arquivo
    if parm_bag_num_palavras_nome_arquivo > 0:
        carrega_modelo_palavras_nome_arquivo(parm_bag_num_palavras_nome_arquivo, parm_metodo_extracao_nome)
        #converte clob para str
        print('antes juntar nome em traduz_nome_arquivo', 'X', X.shape)
        X_nome_arquivo = mod_palavra_nome_arquivo[parm_metodo_extracao_nome].texts_to_matrix(np.array(df.NOME_ARQUIVO), mode=parm_bag_ind_modo_numerico)
        X = np.column_stack((X, X_nome_arquivo))
        print('após juntar nome em traduz_nome_arquivo', 'X', X.shape, ' com X_nome_arquivo',X_nome_arquivo.shape)
        # aplicar log
    return X

mod_palavra_nome_arquivo = {}
mod_palavra_conteudo = {}
mod_redutor_tsvd_768 = None

