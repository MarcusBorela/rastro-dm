import timeit
import datetime
import tensorflow as tf
import numpy as np
from keras import callbacks
from keras.models import Sequential
from keras.layers.core import Dense, Dropout
from keras.callbacks import TensorBoard, EarlyStopping, ModelCheckpoint,ReduceLROnPlateau
from keras import regularizers
from keras.optimizers import  Adadelta
from sklearn.metrics import accuracy_score
from sklearn.model_selection import RepeatedKFold
from rastro import treino_util as cltreino
from cladop import cladop_bd as cladopbd
from numericalizacao import trata_modelo as clu
from rastro import rastro_bd as rastrobd

# Author: marcusborela@yahoo.com.br


def testar_treinamento_rede_treinada_dados_apos_data(parm_cod_treinamento, parm_data_inicio_periodo_treinamento_formato_oracle,parm_kfold=RepeatedKFold(n_splits=7, n_repeats=2, random_state=cltreino.vrandom_state),parm_verbose=0):
    """
    Rotina usada para testar com k-fold alguma execução de treinamento considerando dados após critério definido pela
    data parm_data_inicio_periodo_treinamento_formato_oracle. Esse parâmetro deve ser uma string com a função to_date do oracle.
    Contexto exemplo de chamada: Rotina usada por rotina de monitoramento do cladop. Caso se detecte uma acurácia menor do que a prevista, a rotina deverá inicia novo treinamento passando uma data que atenda requisitos mínimos.  Deve testar acurácia com k-fold do modelo atualmente em produção.
    """
    # tem lista_otimizador e lista_percent_base_validacao a mais do que o outro treina_grid
    if 'to_date' not in parm_data_inicio_periodo_treinamento_formato_oracle.lower():
        raise Exception ('O parâmetro parm_data_inicio_periodo_treinamento_formato_oracle deve ser uma string com a função to_date do oracle.')
    treinamodelo = rastrobd.carregar_parametro_execucao(parm_cod_treinamento) 
    treinamodelo['descr_criterio_selecao']= "TRUNC (doc.data_criacao) > {} and ".format(parm_data_inicio_periodo_treinamento_formato_oracle) + treinamodelo['descr_criterio_selecao']
    treinamodelo['kfold'] = parm_kfold
    return testar_treinamento_rede_dado_parametro(treinamodelo, parm_verbose)

def testar_treinamento_rede_dado_parametro(treinamodelo,  parm_verbose=0):
    """
    Rotina usada para testar com k-fold alguma execução de treinamento
    """
   
    if item_faltando_treinamento_rede_teste_kfold(treinamodelo) != '':
        raise Exception (item_faltando_treinamento_rede_teste_kfold(treinamodelo))
    
    sufixo= '_clf_'+treinamodelo['classificador']+'_modonum_'+treinamodelo['bag_ind_modo_numerico'] +'_detalhe_'+treinamodelo['detalhe_classificador']
    bases = cladopbd.carrega_dados_df_y(treinamodelo['cod_metodo_extracao_texto'], treinamodelo['descr_criterio_selecao'],treinamodelo['qtd_min_por_tipo'],treinamodelo['colunas_extras'])
    treinamodelo['qtd_documento']= bases['df'].COD_DOCUMENTO.count()
    treinamodelo['qtd_tipo']= len(bases['lbTipo'].classes_)
    if treinamodelo['qtd_tipo'] == 1:
        raise Exception ('A quantidade encontrada de tipos (1) impede o uso do algoritmo de classificação')
    X_antes_reduzir = clu.traduz_X_por_bag(np.array(bases['df'].TEXTO), treinamodelo['cod_metodo_extracao_texto'], treinamodelo['bag_num_palavras_conteudo'], treinamodelo['bag_ind_modo_numerico'])
    X_antes_nome = clu.trata_reducao_dimensao(X_antes_reduzir, treinamodelo['redutor_dimensao_conteudo'], treinamodelo['qtd_dimensao_reduzida_conteudo'])
    vcolunas_extras_texto=','.join(treinamodelo['colunas_extras']).strip()
    if vcolunas_extras_texto != '':  # tem colunas
        X_antes_nome = clu.adiciona_colunas_extras(X_antes_nome,treinamodelo['colunas_extras'], bases['df'])
    bases['X'] = clu.traduz_nome_arquivo(X_antes_nome, bases['df'],treinamodelo['bag_num_palavras_nome_arquivo'], 
        treinamodelo['bag_ind_modo_numerico'],treinamodelo['cod_metodo_extracao_nome_arquivo'])
    treinamodelo['qtd_colunas'] = bases['X'].shape[1] 
    if parm_verbose>0:
        print('Parâmetros antes de iniciar treinamento')
        cltreino.imprime_dicionario(treinamodelo)
    treina_modelo_rede_classificacao_bag_kf(treinamodelo['kfold'], bases, treinamodelo, sufixo, parm_verbose)
    return treinamodelo, bases

def testar_treinamento_rede_treinada(parm_cod_treinamento, parm_kfold=RepeatedKFold(n_splits=7, n_repeats=2, random_state=cltreino.vrandom_state), parm_verbose=0):
    """
    Rotina usada para testar com k-fold alguma execução de treinamento
    """
    treinamodelo = rastrobd.carregar_parametro_execucao(parm_cod_treinamento)
    treinamodelo['kfold'] = parm_kfold
    return testar_treinamento_rede_dado_parametro(treinamodelo, parm_verbose)

def gera_classificador_rede_treinada (parm_cod_treinamento, parm_se_usa_validacao=True, parm_verbose=0):
    """
    Rotina para gerar modelo em sua versão final a partir de um registro de treinamento de teste prévio 
    """
    treinamodelo = rastrobd.carregar_parametro_execucao(parm_cod_treinamento)
    if parm_se_usa_validacao: 
        if  'criterio_divisao_base_valid' not in treinamodelo  or 'percent_base_validacao' not in treinamodelo: 
            raise Exception ('Necessário haver parâmtros que configurem a base de validação nos parâmetros')
    else:  # modelo será gerado até época de treinamento experimentada
        del treinamodelo['criterio_divisao_base_valid']
        del treinamodelo['percent_base_validacao']
        treinamodelo['num_epoca_maximo'] = treinamodelo['num_epoca_treina']
    return gera_classificador_rede_dado_parametro (treinamodelo, parm_verbose)

def gera_classificador_rede_treinada_apos_data(parm_cod_treinamento, parm_data_inicio_periodo_treinamento_formato_oracle,parm_kfold=RepeatedKFold(n_splits=7, n_repeats=2, random_state=cltreino.vrandom_state),parm_verbose=0):
    """
    Rotina usada para testar com k-fold alguma execução de treinamento considerando dados após critério definido pela
    data parm_data_inicio_periodo_treinamento_formato_oracle. Esse parâmetro deve ser uma string com a função to_date do oracle.
    Contexto exemplo de chamada: Rotina usada por rotina de monitoramento do cladop. Caso se detecte uma acurácia menor do que a prevista, a rotina deverá inicia novo treinamento
    passando uma data que atenda requisitos mínimos.  Deve testar acurácia com k-fold do modelo atualmente em produção. 
    """
    # tem lista_otimizador e lista_percent_base_validacao a mais do que o outro treina_grid
    if 'to_date' not in parm_data_inicio_periodo_treinamento_formato_oracle.lower():
        raise Exception ('O parâmetro parm_data_inicio_periodo_treinamento_formato_oracle deve ser uma string com a função to_date do oracle.')
    treinamodelo = rastrobd.carregar_parametro_execucao(parm_cod_treinamento) 
    treinamodelo['descr_criterio_selecao']= "TRUNC (doc.data_criacao) > {} and ".format(parm_data_inicio_periodo_treinamento_formato_oracle) + treinamodelo['descr_criterio_selecao']
    return gera_classificador_rede_dado_parametro (treinamodelo, parm_verbose)

def gera_classificador_rede_dado_parametro (treinamodelo, parm_verbose=0):
    """
    Rotina para gerar modelo em sua versão final a partir de parâmetros
    """
    lista_item_a_excluir =  ['kfold', 'criterio_divisao_base_teste', 'percent_base_teste']
    for item in lista_item_a_excluir:
        if item in treinamodelo:  # geração versão não tem base de teste nem kfold
            print('Desconsiderado parâmetro ' + item + ' pois não é necessário na geração da versão final de modelo')
            del treinamodelo[item]
    if item_faltando_treinamento_rede_geracao(treinamodelo) != '':
        raise Exception (item_faltando_treinamento_rede_geracao(treinamodelo))

    sufixo= '_clf_'+treinamodelo['classificador']+'_modonum_'+treinamodelo['bag_ind_modo_numerico'] +'_detalhe_'+treinamodelo['detalhe_classificador']
    bases = cladopbd.carrega_dados_df_y(treinamodelo['cod_metodo_extracao_texto'], treinamodelo['descr_criterio_selecao'],treinamodelo['qtd_min_por_tipo'],treinamodelo['colunas_extras'])
    treinamodelo['qtd_documento']= bases['df'].COD_DOCUMENTO.count()
    treinamodelo['qtd_tipo']= len(bases['lbTipo'].classes_)
    if treinamodelo['qtd_tipo'] == 1:
        raise Exception ('A quantidade encontrada de tipos (1) impede o uso do algoritmo de classificação')
    X_antes_reduzir = clu.traduz_X_por_bag(np.array(bases['df'].TEXTO), treinamodelo['cod_metodo_extracao_texto'], treinamodelo['bag_num_palavras_conteudo'], treinamodelo['bag_ind_modo_numerico'])
    X_antes_nome = clu.trata_reducao_dimensao(X_antes_reduzir, treinamodelo['redutor_dimensao_conteudo'], treinamodelo['qtd_dimensao_reduzida_conteudo'])
    vcolunas_extras_texto=','.join(treinamodelo['colunas_extras']).strip()
    if vcolunas_extras_texto != '':  # tem colunas
        X_antes_nome = clu.adiciona_colunas_extras(X_antes_nome,treinamodelo['colunas_extras'], bases['df'])
    bases['X'] = clu.traduz_nome_arquivo(X_antes_nome, bases['df'],treinamodelo['bag_num_palavras_nome_arquivo'], 
    treinamodelo['bag_ind_modo_numerico'],treinamodelo['cod_metodo_extracao_nome_arquivo'])
    treinamodelo['qtd_colunas'] = bases['X'].shape[1] 
    if parm_verbose>0:
        print('Parâmetros antes de iniciar treinamento')
        cltreino.imprime_dicionario(treinamodelo)
    treinamodelo['modelo_treinado']= treina_modelo_rede_classificacao_bag_total(bases, treinamodelo, sufixo, parm_verbose)
    return treinamodelo, bases




def treina_rede_gridsearch (listas_grid_treinamento, parm_num_epoca_maximo=40, num_iteracao_reinicio=1, parm_verbose=0):
    # tem lista_otimizador e lista_percent_base_validacao a mais do que o outro treina_grid
    treinamodelo = {}
    #bases = {}
    # parâmetros não variáveis
    treinamodelo['classificador']= 'Rede - MLP'
    treinamodelo['num_tamanho_batch'] = 256 #128 #64
    treinamodelo['num_epoca_maximo'] = parm_num_epoca_maximo #40
    treinamodelo['ind_tipo_numericalizacao'] = 'Bag of words'  
    #treinamodelo['lista_se_treinamento_com_shuffle']
    if lista_faltando_grid_treinamento_rede(listas_grid_treinamento) != '':
        raise Exception (lista_faltando_grid_treinamento_rede(listas_grid_treinamento))
    treinamodelo['ind_tipo_numericalizacao'] = 'Bag of words'  
    tot_iteracao = np.prod(np.array([len(x) for x in listas_grid_treinamento.values()]))
    print('tot_iteracao',tot_iteracao)
    cont_iteracao = 0
    for metodo in listas_grid_treinamento['lista_metodo']:
        print('Método *************************************************',metodo)
        treinamodelo['cod_metodo_extracao_texto']= metodo
        for criterio in listas_grid_treinamento['lista_criterio']:
            print('Critério *************************************************',criterio,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
            treinamodelo['descr_criterio_selecao']= criterio
            for qtd_min_por_tipo in listas_grid_treinamento['lista_qtd_min_tipo']:
                print('Qtd min tipo *************************************************',qtd_min_por_tipo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                treinamodelo['qtd_min_por_tipo']= qtd_min_por_tipo
                for colunas_extras in listas_grid_treinamento['colunas_extras']:
                    print('Colunas extras *************************************************',colunas_extras,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                    treinamodelo['colunas_extras'] = colunas_extras
                    bases = cladopbd.carrega_dados_df_y(treinamodelo['cod_metodo_extracao_texto'], treinamodelo['descr_criterio_selecao'],treinamodelo['qtd_min_por_tipo'],treinamodelo['colunas_extras'])
                    treinamodelo['qtd_documento']= bases['df'].COD_DOCUMENTO.count()
                    treinamodelo['qtd_tipo']= len(bases['lbTipo'].classes_)
                    if treinamodelo['qtd_tipo'] == 1:
                        raise Exception ('A quantidade encontrada de tipos (1) impede o uso do algoritmo de classificação')
                    print('Qtd tipos:',treinamodelo['qtd_tipo'])
                    print('Qtd documentos:',treinamodelo['qtd_documento'])
                    for modo_bag in listas_grid_treinamento['lista_modo_bag']:
                        print('Modo bag *************************************************',modo_bag,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                        treinamodelo['bag_ind_modo_numerico'] = modo_bag
                        for tamanho_bag_conteudo in listas_grid_treinamento['lista_tamanho_bag_conteudo']:
                            print('Tamanho bag conteudo *************************************************',tamanho_bag_conteudo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                            treinamodelo['bag_num_palavras_conteudo'] = tamanho_bag_conteudo
                            X_antes_reduzir = clu.traduz_X_por_bag(np.array(bases['df'].TEXTO), treinamodelo['cod_metodo_extracao_texto'], treinamodelo['bag_num_palavras_conteudo'], treinamodelo['bag_ind_modo_numerico'])
                            for redutor_dim in listas_grid_treinamento['redutor_dimensao_conteudo']:
                                treinamodelo['redutor_dimensao_conteudo'] = redutor_dim
                                print('Redutor_dimensao_conteudo *************************************************',treinamodelo['redutor_dimensao_conteudo'],'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                for qtd_dim_reduzida in listas_grid_treinamento['qtd_dimensao_reduzida']:
                                    treinamodelo['qtd_dimensao_reduzida_conteudo'] = qtd_dim_reduzida
                                    print('Qtd_dimensao_reduzida_conteudo *************************************************',treinamodelo['qtd_dimensao_reduzida_conteudo'],'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                    X_antes_nome = clu.trata_reducao_dimensao(X_antes_reduzir, treinamodelo['redutor_dimensao_conteudo'], treinamodelo['qtd_dimensao_reduzida_conteudo'])
                                    vcolunas_extras_texto=','.join(treinamodelo['colunas_extras']).strip()
                                    if vcolunas_extras_texto != '':  # tem colunas
                                        X_antes_nome = clu.adiciona_colunas_extras(X_antes_nome,treinamodelo['colunas_extras'], bases['df'])
                                    for tamanho_bag_nome_arquivo in listas_grid_treinamento['lista_tamanho_bag_nome_arquivo']:
                                        print('Tamanho bag nome_arquivo*************************************************',tamanho_bag_nome_arquivo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                        treinamodelo['bag_num_palavras_nome_arquivo'] = tamanho_bag_nome_arquivo
                                        bases['X'] = clu.traduz_nome_arquivo(X_antes_nome, bases['df'],treinamodelo['bag_num_palavras_nome_arquivo'], 
                                        treinamodelo['bag_ind_modo_numerico'], treinamodelo['cod_metodo_extracao_nome_arquivo'])
                                        treinamodelo['qtd_colunas'] = bases['X'].shape[1] 
                                        if parm_verbose>0:
                                            print("treinamodelo[qtd_colunas]", treinamodelo['qtd_colunas'])
                                        # mudei a partir daqui para unir kfold com validação
                                        for percent_base_validacao in listas_grid_treinamento['lista_percent_base_validacao']:
                                            treinamodelo['percent_base_validacao']= percent_base_validacao
                                            print('Percent_base_validacao *************************************************',percent_base_validacao,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                            for criterio_divisao_base_valid in listas_grid_treinamento['lista_criterio_divisao_base_validacao']:
                                                print('criterio_divisao_base_valid *************************************************',criterio_divisao_base_valid,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                treinamodelo['criterio_divisao_base_valid']=criterio_divisao_base_valid
                                                for se_treinamento_com_shuffle in listas_grid_treinamento['lista_se_treinamento_com_shuffle']:
                                                    print('se_treinamento_com_shuffle *************************************************',str(se_treinamento_com_shuffle),'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                    treinamodelo['se_treinamento_com_shuffle']=se_treinamento_com_shuffle
                                                    if "kfold" in listas_grid_treinamento: # usar k-fold 
                                                        for otimizador in listas_grid_treinamento['lista_otimizador']:
                                                            treinamodelo['nome_otimizador_modelo']= otimizador
                                                            vtexto_otimizador= str(type(treinamodelo['nome_otimizador_modelo'])) + ' Parâmetros: ' + str(treinamodelo['nome_otimizador_modelo'].get_config())
                                                            print(' Otimizador *************************************************',vtexto_otimizador,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                            for detalhe_classificador in listas_grid_treinamento['lista_detalhe_classificador']:
                                                                treinamodelo['detalhe_classificador']= detalhe_classificador
                                                                print(' Detalhe classificador *************************************************',detalhe_classificador)    
                                                                for kf in  listas_grid_treinamento["kfold"]:       
                                                                    treinamodelo['kfold'] =  kf
                                                                    print('KF...', treinamodelo['kfold'],'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                                    sufixo= '_clf_'+treinamodelo['classificador']+'_modonum_'+treinamodelo['bag_ind_modo_numerico'] +'_detalhe_'+treinamodelo['detalhe_classificador']
                                                                    cont_iteracao += 1
                                                                    print('*** Iteracao kfold', cont_iteracao, ' de ', tot_iteracao, ' em ', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                                    if cont_iteracao >= num_iteracao_reinicio:
                                                                        treina_modelo_rede_classificacao_bag_kf(kf, bases, treinamodelo, sufixo, parm_verbose)
                                                                    else:
                                                                        print('*** Saltando Iteracao')

                                                    else:
                                                        for criterio_divisao_base_teste in listas_grid_treinamento['lista_criterio_divisao_base_teste']:
                                                            treinamodelo['criterio_divisao_base_teste']= criterio_divisao_base_teste
                                                            print('Criterio_divisao_bases *************************************************',criterio_divisao_base_teste,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                            for percent_base_teste in listas_grid_treinamento['lista_percent_base_teste']:
                                                                treinamodelo['percent_base_teste']= percent_base_teste
                                                                print('Percent_base_teste *************************************************',percent_base_teste,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                                # dividindo teste
                                                                bases['X_train'], bases['y_train'], bases['y_train_uma_coluna'], bases['X_test'], bases['y_test'], bases['y_test_uma_coluna']=cltreino.divide_2bases_quaisquer(bases['X'],bases['y'],treinamodelo['percent_base_teste'], treinamodelo['criterio_divisao_base_teste'])
                                                                for detalhe_classificador in listas_grid_treinamento['lista_detalhe_classificador']:
                                                                    treinamodelo['detalhe_classificador']= detalhe_classificador
                                                                    print(' Detalhe classificador *************************************************',detalhe_classificador)        
                                                                    for otimizador in listas_grid_treinamento['lista_otimizador']:
                                                                        treinamodelo['nome_otimizador_modelo']= otimizador
                                                                        vtexto_otimizador= str(type(treinamodelo['nome_otimizador_modelo'])) + ' Parâmetros: ' + str(treinamodelo['nome_otimizador_modelo'].get_config())
                                                                        print(' Otimizador *************************************************',vtexto_otimizador,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                                        cont_iteracao += 1
                                                                        print('*** Iteracao', cont_iteracao, ' de ', tot_iteracao, ' em ', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                                        if cont_iteracao >= num_iteracao_reinicio:
                                                                            sufixo = '_clf_'+treinamodelo['classificador']+'_modonum_'+treinamodelo['bag_ind_modo_numerico'] +'_detalhe_'+treinamodelo['detalhe_classificador']
                                                                            treina_modelo_rede_classificacao_bag(bases, treinamodelo, sufixo, parm_verbose)
                                                                        else:
                                                                            print('*** Saltando Iteracao')
    return treinamodelo, bases


def gera_classificador_rede_grid (listas_grid_treinamento, parm_num_epoca_maximo=40,  parm_verbose=0):
    # tem lista_otimizador e lista_percent_base_validacao a mais do que o outro treina_grid
    treinamodelo = {}
    #bases = {}
    # parâmetros não variáveis
    treinamodelo['classificador']= 'Rede - MLP'
    treinamodelo['num_tamanho_batch'] = 256 #128 #64
    treinamodelo['num_epoca_maximo'] = parm_num_epoca_maximo #40
    treinamodelo['ind_tipo_numericalizacao'] = 'Bag of words'  
    if lista_faltando_parametro_gera_rede(listas_grid_treinamento) != '':
        raise Exception (lista_faltando_parametro_gera_rede(listas_grid_treinamento))
    treinamodelo['ind_tipo_numericalizacao'] = 'Bag of words'  
    tot_iteracao = np.prod(np.array([len(x) for x in listas_grid_treinamento.values()]))
    assert tot_iteracao == 1
    assert 'kfold' not in listas_grid_treinamento
    if 'lista_criterio_divisao_base_validacao' in listas_grid_treinamento:  # não programada variação em grid desse parâmetro
        treinamodelo['criterio_divisao_base_valid']= listas_grid_treinamento['lista_criterio_divisao_base_validacao'][0] 
    for metodo in listas_grid_treinamento['lista_metodo']:
        print('Método *************************************************',metodo)
        treinamodelo['cod_metodo_extracao_texto']= metodo
        for criterio in listas_grid_treinamento['lista_criterio']:
            print('Critério *************************************************',criterio,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
            treinamodelo['descr_criterio_selecao']= criterio
            for qtd_min_por_tipo in listas_grid_treinamento['lista_qtd_min_tipo']:
                print('Qtd min tipo *************************************************',qtd_min_por_tipo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                treinamodelo['qtd_min_por_tipo']= qtd_min_por_tipo
                for colunas_extras in listas_grid_treinamento['colunas_extras']:
                    print('Colunas extras *************************************************',colunas_extras,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                    treinamodelo['colunas_extras'] = colunas_extras
                    bases = cladopbd.carrega_dados_df_y(treinamodelo['cod_metodo_extracao_texto'], treinamodelo['descr_criterio_selecao'],treinamodelo['qtd_min_por_tipo'],treinamodelo['colunas_extras'])
                    treinamodelo['qtd_documento']= bases['df'].COD_DOCUMENTO.count()
                    treinamodelo['qtd_tipo']= len(bases['lbTipo'].classes_)
                    if treinamodelo['qtd_tipo'] == 1:
                        raise Exception ('A quantidade encontrada de tipos (1) impede o uso do algoritmo de classificação')
                    print('Qtd tipos:',treinamodelo['qtd_tipo'])
                    print('Qtd documentos:',treinamodelo['qtd_documento'])
                    for modo_bag in listas_grid_treinamento['lista_modo_bag']:
                        print('Modo bag *************************************************',modo_bag,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                        treinamodelo['bag_ind_modo_numerico'] = modo_bag
                        for tamanho_bag_conteudo in listas_grid_treinamento['lista_tamanho_bag_conteudo']:
                            print('Tamanho bag conteudo *************************************************',tamanho_bag_conteudo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                            treinamodelo['bag_num_palavras_conteudo'] = tamanho_bag_conteudo
                            X_antes_reduzir = clu.traduz_X_por_bag(np.array(bases['df'].TEXTO), treinamodelo['cod_metodo_extracao_texto'], treinamodelo['bag_num_palavras_conteudo'], treinamodelo['bag_ind_modo_numerico'])
                            for redutor_dim in listas_grid_treinamento['redutor_dimensao_conteudo']:
                                treinamodelo['redutor_dimensao_conteudo'] = redutor_dim
                                print('Redutor_dimensao_conteudo *************************************************',treinamodelo['redutor_dimensao_conteudo'],'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                for qtd_dim_reduzida in listas_grid_treinamento['qtd_dimensao_reduzida']:
                                    treinamodelo['qtd_dimensao_reduzida'] = qtd_dim_reduzida
                                    print('Qtd_dimensao_reduzida *************************************************',treinamodelo['qtd_dimensao_reduzida'],'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                    X_antes_nome = clu.trata_reducao_dimensao(X_antes_reduzir, treinamodelo['redutor_dimensao_conteudo'], treinamodelo['qtd_dimensao_reduzida_conteudo'])
                                    vcolunas_extras_texto=','.join(treinamodelo['colunas_extras']).strip()
                                    if vcolunas_extras_texto != '':  # tem colunas
                                        X_antes_nome = clu.adiciona_colunas_extras(X_antes_nome,treinamodelo['colunas_extras'], bases['df'])
                                    for tamanho_bag_nome_arquivo in listas_grid_treinamento['lista_tamanho_bag_nome_arquivo']:
                                        print('Tamanho bag nome_arquivo*************************************************',tamanho_bag_nome_arquivo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                        treinamodelo['bag_num_palavras_nome_arquivo'] = tamanho_bag_nome_arquivo
                                        bases['X'] = clu.traduz_nome_arquivo(X_antes_nome, bases['df'],treinamodelo['bag_num_palavras_nome_arquivo'], 
                                        treinamodelo['bag_ind_modo_numerico'], treinamodelo['cod_metodo_extracao_nome_arquivo'])
                                        treinamodelo['qtd_colunas'] = bases['X'].shape[1] 
                                        if parm_verbose>0:
                                            print("treinamodelo[qtd_colunas]", treinamodelo['qtd_colunas'])
                                        # mudei a partir daqui para unir kfold com validação
                                        for percent_base_validacao in listas_grid_treinamento['lista_percent_base_validacao']:
                                            treinamodelo['percent_base_validacao']= percent_base_validacao
                                            print('Percent_base_validacao *************************************************',percent_base_validacao,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                            for se_treinamento_com_shuffle in listas_grid_treinamento['lista_se_treinamento_com_shuffle']:
                                                print('se_treinamento_com_shuffle *************************************************',str(se_treinamento_com_shuffle),'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                treinamodelo['se_treinamento_com_shuffle']=se_treinamento_com_shuffle
                                                for detalhe_classificador in listas_grid_treinamento['lista_detalhe_classificador']:
                                                    treinamodelo['detalhe_classificador']= detalhe_classificador
                                                    print(' Detalhe classificador *************************************************',detalhe_classificador)        
                                                    for otimizador in listas_grid_treinamento['lista_otimizador']:
                                                        treinamodelo['nome_otimizador_modelo']= otimizador
                                                        vtexto_otimizador= str(type(treinamodelo['nome_otimizador_modelo'])) + ' Parâmetros: ' + str(treinamodelo['nome_otimizador_modelo'].get_config())
                                                        print(' Otimizador *************************************************',vtexto_otimizador,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                        sufixo = '_clf_'+treinamodelo['classificador']+'_modonum_'+treinamodelo['bag_ind_modo_numerico'] +'_detalhe_'+treinamodelo['detalhe_classificador']
                                                        treinamodelo['modelo_treinado']= treina_modelo_rede_classificacao_bag_total(bases, treinamodelo, sufixo, parm_verbose)
        return treinamodelo, bases


def retorna_splits_kf(kf,parm_X,parm_y):
    """
       Tem kfs estratificados que exigem X e y, por isso o except
    """
    try: 
        return kf.split(parm_y)
    except:
        return kf.split(parm_X, parm_y)

def treina_modelo_rede_classificacao_bag_kf(kf, bases, treinamodelo, parm_sufixo, parm_verbose=0):

    #detalhe_modelo = treinamodelo['detalhe_classificador']
    parm_num_epoca_maximo = treinamodelo['num_epoca_maximo']
    parm_num_tamanho_batch = treinamodelo['num_tamanho_batch']

    agora = datetime.datetime.now().strftime("%Y%m%d_%H_%M_%S") 
    #callbacks.EarlyStopping(monitor='loss', patience=4, verbose=1, mode='min'),
    try:  
        n_folds = kf.n_splits
    except: 
        n_folds = kf.get_n_splits(X=bases['y_uma_coluna'])
    acuracia_treinamento=np.empty((n_folds))
    parm_patience=15
    acuracia_teste=np.empty((n_folds))
    acuracia_valid=np.empty((n_folds))
    num_epoca_treina=np.empty((n_folds))
    cltreino.imprime_treinamodelo(treinamodelo)
    for i, (train_ndx, test_ndx) in enumerate(retorna_splits_kf(kf,bases['X'],bases['y_uma_coluna'])):
        txt_passo= 'Treinando o modelo em fold ' + str(i+1) + "/" + str(n_folds)
        print(txt_passo)
        bases['X_train']= bases['X'][train_ndx]
        bases['y_train']= bases['y'][train_ndx]
        bases['X_test']= bases['X'][test_ndx]
        bases['y_test']=bases['y'][test_ndx]
        bases['y_test_uma_coluna']= [np.argmax(t) for t in bases['y_test']]
        # dividindo validacao
        bases['X_train'], bases['y_train'], bases['y_train_uma_coluna'], bases['X_val'], bases['y_val'], bases['y_val_uma_coluna']=cltreino.divide_2bases_quaisquer(bases['X_train'],bases['y_train'],treinamodelo['percent_base_validacao'], treinamodelo['criterio_divisao_base_valid'])
        if parm_verbose > 0:
            print('bases["X_train"].shape', bases['X_train'].shape)
            print('bases["y_train"].shape', bases['y_train'].shape)
            print('bases["X_test"].shape', bases['X_test'].shape)
            print('bases["y_test"].shape', bases['y_test'].shape)
            print('bases["X_val"].shape', bases['X_val'].shape)
            print('bases["y_val"].shape', bases['y_val'].shape)

        model = build_model_rede(treinamodelo)
        if parm_verbose > 0 and i == 1: # só imprime uma vez
            print('modelo', model.summary())

        if 'categorical_accuracy' in model.metrics:
            callback_lista = [
                callbacks.EarlyStopping(monitor='val_categorical_accuracy', patience=parm_patience, verbose=parm_verbose, mode='max'),
                callbacks.TensorBoard(log_dir=cltreino.get_path_modelo() + 'logtensorboard/logmodel_'+parm_sufixo+'_{}'.format(agora), histogram_freq=0, write_graph=True, write_images=True),
                callbacks.ModelCheckpoint(filepath=cltreino.get_path_modelo() + 'Model_checkpoint_'+parm_sufixo+'_{}.hdf5'.format(agora), monitor='val_categorical_accuracy', verbose=1, save_best_only=True, mode='max'),
                ReduceLROnPlateau(patience=4,verbose=1,monitor='val_categorical_accuracy',factor=0.5,min_lr=0.0001, mode='max')
                ]
        elif 'accuracy' in model.metrics:
            callback_lista = [
                        callbacks.EarlyStopping(monitor='val_acc', patience=parm_patience, verbose=1, mode='max'),
                        callbacks.TensorBoard(log_dir=cltreino.get_path_modelo() + 'logtensorboard/logmodel_'+parm_sufixo+'_{}'.format(agora), histogram_freq=0, write_graph=True, write_images=True),
                        callbacks.ModelCheckpoint(filepath=cltreino.get_path_modelo() + 'Model_checkpoint_'+parm_sufixo+'_{}.hdf5'.format(agora), monitor='val_acc', verbose=1, save_best_only=True, mode='max'),
                        ReduceLROnPlateau(patience=4,verbose=1,monitor='val_acc',factor=0.5,min_lr=0.0001, mode='max')
                        ]
        else:
            raise Exception ('Métrica do modelo não prevista')



        start = timeit.default_timer()

        #vcom_shuffle= 'shuffle' in detalhe_modelo
        vhistory = model.fit(bases['X_train'], bases['y_train'], epochs=parm_num_epoca_maximo, batch_size=parm_num_tamanho_batch, verbose=parm_verbose, callbacks= callback_lista, validation_data=(bases['X_val'], bases['y_val']), shuffle=treinamodelo['se_treinamento_com_shuffle'])

        stop = timeit.default_timer()
        treinamodelo['tempo_execucao'] = stop - start

        # Apurando resultados
        # Apurando resultados
        #txt_passo= 'Apurando resultados...'
        #print(txt_passo)

        if 'acc' in vhistory.history:
            acuracia_treinamento[i] = max(vhistory.history['acc'])
        else:
            acuracia_treinamento[i] =  max(vhistory.history['categorical_accuracy'])

        if 'val_acc' in vhistory.history:
            acuracia_valid[i] = max(vhistory.history['val_acc'])
            num_epoca_treina[i] = len(vhistory.history['val_acc'])
        else:
            acuracia_valid[i] = max(vhistory.history['val_categorical_accuracy'])
            num_epoca_treina[i] = len(vhistory.history['val_categorical_accuracy'])

        if num_epoca_treina[i] < parm_num_epoca_maximo:
            num_epoca_treina[i] -= parm_patience

        if parm_verbose > 0:
            print('percent_base_validacao', treinamodelo['percent_base_validacao'])
        y_pred_test = model.predict_classes(bases['X_test'])
        acuracia_teste[i] = accuracy_score(bases['y_test_uma_coluna'],y_pred_test)
        

        print('Acurácia alcançada em fold:', i+1, "em treinamento:", acuracia_treinamento[i], "em validação:", acuracia_valid[i], "em teste:", acuracia_teste[i], " Número de épocas:",num_epoca_treina[i], datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))





    treinamodelo['num_epoca_treina'] = num_epoca_treina.mean()
    treinamodelo['percent_acuracia_teste'] = acuracia_teste.mean()
    treinamodelo['percent_acuracia_teste_std'] = acuracia_teste.std()
    treinamodelo['percent_acuracia_valida'] = acuracia_valid.mean() 
    treinamodelo['percent_acuracia_valida_std'] = acuracia_valid.std() 
    treinamodelo['percent_acuracia_treina'] = acuracia_treinamento.mean() 
    treinamodelo['percent_acuracia_treina_std'] = acuracia_treinamento.std() 
    print('Acurácia alcançada em kfold - teste', treinamodelo['percent_acuracia_teste'], ' std de ', treinamodelo['percent_acuracia_teste_std'])    
    print('                        treinamento', treinamodelo['percent_acuracia_treina'], ' std de ', treinamodelo['percent_acuracia_treina_std'])    
    print('                        validação', treinamodelo['percent_acuracia_valida'], ' std de ', treinamodelo['percent_acuracia_valida_std'])    

    treinamodelo['cod_registro_treinamento'] = rastrobd.insere_resultado(treinamodelo)
    return

def treina_modelo_rede_classificacao_bag_total(bases, treinamodelo, parm_sufixo, parm_verbose=0):
    """
    Recebe parâmetros de treinamento em treinamodelo
    E tipos existentes (cod, nome) em dfTipos
    E mod_palavras com o modelos de palavras (Tokenizer) gerado antes
    """
    #detalhe_modelo = treinamodelo['detalhe_classificador']
    parm_num_epoca_maximo = treinamodelo['num_epoca_maximo']
    parm_num_tamanho_batch = treinamodelo['num_tamanho_batch']
    agora = datetime.datetime.now().strftime("%Y%m%d_%H_%M_%S") 

    txt_passo= 'Construindo e compilando o modelo...'
    print(txt_passo)
    model = build_model_rede(treinamodelo)
    txt_passo= 'Treinando o modelo...'
    print(txt_passo)

    start = timeit.default_timer()
    #vcom_shuffle= 'shuffle' in detalhe_modelo
    if 'percent_base_validacao' in treinamodelo:
        if treinamodelo ['percent_base_validacao'] < 0:
            raise Exception ('percent_base_validacao não informado e necessário para treinamento')
        parm_patience=15
        if 'categorical_accuracy' in model.metrics:
            callback_lista = [
                callbacks.EarlyStopping(monitor='val_categorical_accuracy', patience=parm_patience, verbose=1, mode='max'),
                callbacks.TensorBoard(log_dir=cltreino.get_path_modelo() + 'logtensorboard/logmodel_'+parm_sufixo+'_{}'.format(agora), histogram_freq=0, write_graph=True, write_images=True),
                callbacks.ModelCheckpoint(filepath=cltreino.get_path_modelo() + 'Model_checkpoint_'+parm_sufixo+'_{}.hdf5'.format(agora), monitor='val_categorical_accuracy', verbose=1, save_best_only=True, mode='max'),
                ReduceLROnPlateau(patience=4,verbose=1,monitor='val_categorical_accuracy',factor=0.5,min_lr=0.0001, mode='max')
                ]
        elif 'accuracy' in model.metrics:
            callback_lista = [
                        callbacks.EarlyStopping(monitor='val_acc', patience=parm_patience, verbose=1, mode='max'),
                        callbacks.TensorBoard(log_dir=cltreino.get_path_modelo() + 'logtensorboard/logmodel_'+parm_sufixo+'_{}'.format(agora), histogram_freq=0, write_graph=True, write_images=True),
                        callbacks.ModelCheckpoint(filepath=cltreino.get_path_modelo() + 'Model_checkpoint_'+parm_sufixo+'_{}.hdf5'.format(agora), monitor='val_acc', verbose=1, save_best_only=True, mode='max'),
                        ReduceLROnPlateau(patience=4,verbose=1,monitor='val_acc',factor=0.5,min_lr=0.0001, mode='max')
                        ]
        # dividindo validacao
        bases['X_train'], bases['y_train'], bases['y_train_uma_coluna'], bases['X_val'], bases['y_val'], bases['y_val_uma_coluna']=cltreino.divide_2bases_quaisquer(bases['X'],bases['y'],treinamodelo['percent_base_validacao'], treinamodelo['criterio_divisao_base_valid'])

        if parm_verbose > 0:
            print('bases["X_train"].shape', bases['X_train'].shape)
            print('bases["y_train"].shape', bases['y_train'].shape)
            print('bases["X_val"].shape', bases['X_val'].shape)
            print('bases["y_val"].shape', bases['y_val'].shape)
        vhistory = model.fit(bases['X_train'], bases['y_train'], epochs=parm_num_epoca_maximo, batch_size=parm_num_tamanho_batch, verbose=parm_verbose, callbacks= callback_lista, validation_data=(bases['X_val'], bases['y_val']), shuffle=treinamodelo['se_treinamento_com_shuffle'])
        if 'val_acc' in vhistory.history:
            treinamodelo['percent_acuracia_valida'] = max(vhistory.history['val_acc'])
            treinamodelo['num_epoca_treina'] = len(vhistory.history['val_acc'])
        else:
            treinamodelo['percent_acuracia_valida'] = max(vhistory.history['val_categorical_accuracy'])
            treinamodelo['num_epoca_treina'] = len(vhistory.history['val_categorical_accuracy'])

        if treinamodelo['num_epoca_treina'] < parm_num_epoca_maximo:
            treinamodelo['num_epoca_treina'] -= parm_patience
        print('Acurácia em validação:', treinamodelo['percent_acuracia_valida'])
    else:
        if 'categorical_accuracy' in model.metrics:
            callback_lista = [
                callbacks.TensorBoard(log_dir=cltreino.get_path_modelo()+'logtensorboard/logmodel_'+parm_sufixo+'_{}'.format(agora), histogram_freq=0, write_graph=True, write_images=True),
                callbacks.ModelCheckpoint(filepath=cltreino.get_path_modelo() + 'Model_checkpoint_'+parm_sufixo+'_{}.hdf5'.format(agora), monitor='categorical_accuracy', verbose=1, save_best_only=True, mode='max'),
                ]
        elif 'accuracy' in model.metrics:
            callback_lista = [
                        callbacks.TensorBoard(log_dir=cltreino.get_path_modelo() + 'logtensorboard/logmodel_'+parm_sufixo+'_{}'.format(agora), histogram_freq=0, write_graph=True, write_images=True),
                        callbacks.ModelCheckpoint(filepath=cltreino.get_path_modelo() + 'Model_checkpoint_'+parm_sufixo+'_{}.hdf5'.format(agora), monitor='acc', verbose=1, save_best_only=True, mode='max'),
                        ]
        else:
            raise Exception ('Métrica do modelo não prevista')
        vhistory = model.fit(bases['X'], bases['y'], epochs=parm_num_epoca_maximo, batch_size=parm_num_tamanho_batch, verbose=parm_verbose, callbacks= callback_lista, shuffle=treinamodelo['se_treinamento_com_shuffle'])
        treinamodelo['num_epoca_treina'] = parm_num_epoca_maximo
    stop = timeit.default_timer()
    treinamodelo['tempo_execucao'] = stop - start

    # Apurando resultados
    txt_passo= 'Apurando resultados...'
    print(txt_passo)

    if 'acc' in vhistory.history:
        treinamodelo['percent_acuracia_treina'] = max(vhistory.history['acc'])
    else:
        treinamodelo['percent_acuracia_treina'] =  max(vhistory.history['categorical_accuracy'])

    print('Acurácia em treinamento:', treinamodelo['percent_acuracia_treina'],' Número de épocas:', treinamodelo['num_epoca_treina'])
   
    # salva resultado no banco de dados
    treinamodelo['cod_registro_treinamento'] = rastrobd.insere_resultado(treinamodelo)

    return model

def treina_modelo_rede_classificacao_bag(bases, treinamodelo, parm_sufixo, parm_verbose=0):
    """
    Recebe parâmetros de treinamento em treinamodelo
    E tipos existentes (cod, nome) em dfTipos
    E mod_palavras com o modelos de palavras (Tokenizer) gerado antes
    """
    # detalhe_modelo = treinamodelo['detalhe_classificador']
    parm_num_epoca_maximo = treinamodelo['num_epoca_maximo']
    parm_num_tamanho_batch = treinamodelo['num_tamanho_batch']
    agora = datetime.datetime.now().strftime("%Y%m%d_%H_%M_%S") 

    txt_passo= 'Construindo e compilando o modelo...'
    print(txt_passo)
    model = build_model_rede(treinamodelo)
    txt_passo= 'Treinando o modelo...'
    print(txt_passo)
    parm_patience=15
    if 'categorical_accuracy' in model.metrics:
        callback_lista = [
              callbacks.EarlyStopping(monitor='val_categorical_accuracy', patience=parm_patience, verbose=1, mode='max'),
              callbacks.TensorBoard(log_dir=cltreino.get_path_modelo() + 'logtensorboard/logmodel_'+parm_sufixo+'_{}'.format(agora), histogram_freq=0, write_graph=True, write_images=True),
              callbacks.ModelCheckpoint(filepath=cltreino.get_path_modelo() + 'Model_checkpoint_'+parm_sufixo+'_{}.hdf5'.format(agora), monitor='val_categorical_accuracy', verbose=1, save_best_only=True, mode='max'),
              ReduceLROnPlateau(patience=4,verbose=1,monitor='val_categorical_accuracy',factor=0.5,min_lr=0.0001, mode='max')
             ]
    elif 'accuracy' in model.metrics:
        callback_lista = [
                      callbacks.EarlyStopping(monitor='val_acc', patience=parm_patience, verbose=1, mode='max'),
                      callbacks.TensorBoard(log_dir=cltreino.get_path_modelo() + 'logtensorboard/logmodel_'+parm_sufixo+'_{}'.format(agora), histogram_freq=0, write_graph=True, write_images=True),
                      callbacks.ModelCheckpoint(filepath=cltreino.get_path_modelo() + 'Model_checkpoint_'+parm_sufixo+'_{}.hdf5'.format(agora), monitor='val_acc', verbose=1, save_best_only=True, mode='max'),
                      ReduceLROnPlateau(patience=4,verbose=1,monitor='val_acc',factor=0.5,min_lr=0.0001, mode='max')
                     ]
    else:
        raise Exception ('Métrica do modelo não prevista')
    

    start = timeit.default_timer()
    #callbacks.EarlyStopping(monitor='loss', patience=4, verbose=1, mode='min'),
    bases['X_train'], bases['y_train'], bases['y_train_uma_coluna'], bases['X_val'], bases['y_val'], bases['y_val_uma_coluna']=cltreino.divide_2bases_quaisquer(bases['X_train'],bases['y_train'],treinamodelo['percent_base_validacao'], treinamodelo['criterio_divisao_base_valid'])
    if parm_verbose > 0:
        print('bases["X_train"].shape', bases['X_train'].shape)
        print('bases["y_train"].shape', bases['y_train'].shape)
        print('bases["X_test"].shape', bases['X_test'].shape)
        print('bases["y_test"].shape', bases['y_test'].shape)
        print('bases["X_val"].shape', bases['X_val'].shape)
        print('bases["y_val"].shape', bases['y_val'].shape)

    #vcom_shuffle= 'shuffle' in detalhe_modelo
    vhistory = model.fit(bases['X_train'], bases['y_train'], epochs=parm_num_epoca_maximo, batch_size=parm_num_tamanho_batch, verbose=parm_verbose, callbacks= callback_lista, validation_data=(bases['X_val'], bases['y_val']), shuffle=treinamodelo['se_treinamento_com_shuffle'])

    stop = timeit.default_timer()
    treinamodelo['tempo_execucao'] = stop - start

    # Apurando resultados
    txt_passo= 'Apurando resultados...'
    print(txt_passo)


    if 'acc' in vhistory.history:
        treinamodelo['percent_acuracia_treina'] = max(vhistory.history['acc'])
    else:
        treinamodelo['percent_acuracia_treina'] =  max(vhistory.history['categorical_accuracy'])

    y_pred_test = model.predict_classes(bases['X_test'])
    treinamodelo['percent_acuracia_teste'] = accuracy_score(bases['y_test_uma_coluna'],y_pred_test)


    if 'val_acc' in vhistory.history:
        treinamodelo['percent_acuracia_valida'] = max(vhistory.history['val_acc'])
        treinamodelo['num_epoca_treina'] = len(vhistory.history['val_acc'])
    else:
        treinamodelo['percent_acuracia_valida'] = max(vhistory.history['val_categorical_accuracy'])
        treinamodelo['num_epoca_treina'] = len(vhistory.history['val_categorical_accuracy'])

    if treinamodelo['num_epoca_treina'] < parm_num_epoca_maximo:
        treinamodelo['num_epoca_treina'] -= parm_patience

    print('Acurácia alcançada em teste:', treinamodelo['percent_acuracia_teste'], 'Em validação:', treinamodelo['percent_acuracia_valida'], 'Em treinamento:', treinamodelo['percent_acuracia_treina'],' Número de épocas:', treinamodelo['num_epoca_treina'])
   

    # salva resultado no banco de dados
    treinamodelo['cod_registro_treinamento'] = rastrobd.insere_resultado(treinamodelo)

    """
    y_pred_train = model.predict_classes(bases['X_train'])
    #treinamodelo['percent_acuracia_treina'] = accuracy_score(bases['y_train_uma_coluna'],y_pred_train)
    if parm_se_gera_graficos:

        txt_passo=  'Calculando matriz de confusão e plotando resultados'
        print(txt_passo)
        cnf_matrix_test = confusion_matrix(bases['y_test_uma_coluna'], y_pred_test)
        cnf_matrix_train = confusion_matrix(bases['y_train_uma_coluna'], y_pred_train)


        print('Gráfico do treinamento nas diversas épocas: treinamento x validação')
        plot_history(vhistory)

        print('Avaliação do modelo com dados de treinamento - por tipo')
        plot_confusion_matrix(cnf_matrix_train,classes=bases['Labels'], cmap='YlGn', normalize=False)
        print(classification_report(bases['y_train_uma_coluna'],y_pred_train,digits=5))   

        print('Avaliação do modelo com dados de teste - por tipo')
        plot_confusion_matrix(cnf_matrix_test,classes=bases['Labels'], cmap='YlGn', normalize=False)
        print(classification_report(bases['y_test_uma_coluna'],y_pred_test,digits=5))    
    """

    return 


def gera_listas_grid_treinamento_rede(parm_tipo='sem_Kfold'): 
    listas_grid_treinamento = cltreino.gera_listas_grid_treinamento(parm_tipo)
    
    listas_grid_treinamento['lista_otimizador'] = [Adadelta(lr = 1, rho = 0.95, decay = 0)]
    """
    Adam(lr=0.001,amsgrad=True),
    Adam(lr=0.001,amsgrad=False),
    RMSprop(lr=0.001), 
    Adadelta(lr = 1, rho = 0.95, decay = 0)
    """
    #adagrad
    # tirado: sgd rmsprop - resultado baixo
    # 
    if parm_tipo=='sem_kfold':
        listas_grid_treinamento['lista_criterio_divisao_base_teste'] = ['sem estratificacao, com shuffle']        

    listas_grid_treinamento['lista_criterio_divisao_base_validacao'] = ['sem estratificacao, com shuffle']        
    listas_grid_treinamento['lista_percent_base_validacao'] = [0.2]
    listas_grid_treinamento['lista_detalhe_classificador'] = ["A- validacao estratificada, rede 2 camadas (2048 1024_relu_dropout60 reduceLR shuffle metrica categorical_accuracy"]
    return listas_grid_treinamento

def item_faltando_treinamento_rede_teste_kfold(parm_treinamodelo):
    esperado = lista_parametro_geracao_rede + ['kfold', 'criterio_divisao_base_valid', 'percent_base_validacao']
    faltando = str([x for x in esperado if x not in parm_treinamodelo]) if len([x for x in esperado if x not in parm_treinamodelo ]) > 1 else ''
    if ('percent_base_teste' in parm_treinamodelo) or ('criterio_divisao_base_teste' in parm_treinamodelo):
        faltando += ' Erro: percent_base_teste ou criterio_divisao_base_teste não podem ser definidos ao mesmo tempo de kfold'
    return str(faltando)

def item_faltando_treinamento_rede_geracao(parm_treinamodelo):
    esperado = lista_parametro_geracao_rede
    faltando = str([x for x in esperado if x not in parm_treinamodelo]) if len([x for x in esperado if x not in parm_treinamodelo ]) > 1 else ''
    if  ('kfold' in parm_treinamodelo) or ('percent_base_teste' in parm_treinamodelo) or 'criterio_divisao_base_teste' in parm_treinamodelo:
        faltando += ' Erro: kfold, percent_base_teste ou criterio_divisao_base_teste não podem ser definidos para geração'
    return str(faltando)

def lista_faltando_grid_treinamento_rede(listas_grid_treinamento):
    listas_esperadas = ['lista_metodo', 'lista_se_treinamento_com_shuffle', 'lista_criterio', 'lista_qtd_min_tipo', 'lista_modo_bag', 'lista_tamanho_bag_conteudo','lista_tamanho_bag_nome_arquivo',  'lista_detalhe_classificador', 'lista_otimizador', 'lista_percent_base_validacao','lista_criterio_divisao_base_validacao'] 
    listas_faltando = str([x for x in listas_esperadas if x not in listas_grid_treinamento ]) if len([x for x in listas_esperadas if x not in listas_grid_treinamento ]) > 1 else ''
    if 'kfold' in listas_grid_treinamento and ('lista_percent_base_teste' in listas_grid_treinamento or 'lista_criterio_divisao_base_teste' in listas_grid_treinamento):
        listas_faltando += ' Erro: lista_percent_base_teste ou lista_criterio_divisao_base_teste não podem ser definida ao mesmo tempo de kfold'
    if 'kfold' not in listas_grid_treinamento and 'lista_percent_base_teste' not in listas_grid_treinamento and 'lista_criterio_divisao_base_teste' not in listas_grid_treinamento:
        listas_faltando += ' Erro: kfold ou (lista_percent_base_teste, lista_criterio_divisao_base_teste) deve ser definido'
    return str(listas_faltando)

def lista_faltando_parametro_gera_rede(listas_grid_treinamento):
    listas_esperadas = ['lista_metodo', 'lista_se_treinamento_com_shuffle', 'lista_criterio', 'lista_qtd_min_tipo', 'lista_modo_bag', 'lista_tamanho_bag_conteudo','lista_tamanho_bag_nome_arquivo',  'lista_detalhe_classificador', 'lista_otimizador', 'lista_percent_base_validacao'] 
    listas_faltando = str([x for x in listas_esperadas if x not in listas_grid_treinamento ]) if len([x for x in listas_esperadas if x not in listas_grid_treinamento ]) > 1 else ''
    if ('lista_criterio_divisao_base_validacao' in listas_grid_treinamento) != ('lista_percent_base_validacao' in listas_grid_treinamento):
        listas_faltando += ' Ou tem ou não ambos:criterio_divisao_base_validacao e lista_percent_base_validacao'
    return str(listas_faltando)

def build_model_rede(treinamodelo):
    # Compilando o modelo
    modelo = treinamodelo['detalhe_classificador']
    qtd_colunas = treinamodelo['qtd_colunas']
    print('qtd_colunas em build_model_rede', qtd_colunas)

    if modelo[0] in ['2', '1', '3']: # 1 - validacao estratificada, rede 3 camadas (1024_relu_dropout50)', \
        #'1 - rede 3 camadas (1024_relu_dropout50)',
        #                     '3 - validacao estratificada, rede 3 camadas (1024_relu_dropout50_reduceLR'
        model = Sequential()
        model.add(Dense(1024, input_shape=(qtd_colunas,), activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(treinamodelo['qtd_tipo'], activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=treinamodelo['nome_otimizador_modelo'],  metrics=["categorical_accuracy"])
    elif modelo[0] == '4':
        model = Sequential()
        model.add(Dense(1024, input_shape=(qtd_colunas,), activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='relu',kernel_regularizer=regularizers.l1(0.01)))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='relu',kernel_regularizer=regularizers.l1(0.01)))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='relu',kernel_regularizer=regularizers.l1(0.01)))
        model.add(Dropout(0.5))
        model.add(Dense(treinamodelo['qtd_tipo'], activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=treinamodelo['nome_otimizador_modelo'],  metrics=["categorical_accuracy"])
    elif modelo[0] == '5':
        model = Sequential()
        model.add(Dense(1024, input_shape=(qtd_colunas,), activation='relu'))
        model.add(Dropout(0.6))
        model.add(Dense(1024, activation='relu'))
        model.add(Dropout(0.6))
        model.add(Dense(1024, activation='relu'))
        model.add(Dropout(0.6))
        model.add(Dense(1024, activation='relu'))
        model.add(Dropout(0.6))
        model.add(Dense(treinamodelo['qtd_tipo'], activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=treinamodelo['nome_otimizador_modelo'],  metrics=["categorical_accuracy"])
    elif modelo[0] == '6':
        model = Sequential()
        model.add(Dense(1024, input_shape=(qtd_colunas,), activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='tanh'))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='tanh'))
        model.add(Dropout(0.5))
        model.add(Dense(treinamodelo['qtd_tipo'], activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=treinamodelo['nome_otimizador_modelo'],  metrics=["categorical_accuracy"])
    elif modelo[0] == '7':
        model = Sequential()
        model.add(Dense(1024, input_shape=(qtd_colunas,), activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='sigmoid'))
        model.add(Dropout(0.5))
        model.add(Dense(treinamodelo['qtd_tipo'], activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=treinamodelo['nome_otimizador_modelo'],  metrics=["categorical_accuracy"])
    elif modelo[0] == '8':
        model = Sequential()
        model.add(Dense(1024, input_shape=(qtd_colunas,), activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='tanh'))
        model.add(Dropout(0.5))
        model.add(Dense(treinamodelo['qtd_tipo'], activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=treinamodelo['nome_otimizador_modelo'],  metrics=["categorical_accuracy"])
    elif modelo[0] == '9':
        model = Sequential()
        model.add(Dense(1024, input_shape=(qtd_colunas,), activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(1024, activation='tanh'))
        model.add(Dropout(0.5))
        model.add(Dense(treinamodelo['qtd_tipo'], activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=treinamodelo['nome_otimizador_modelo'],  metrics=["categorical_accuracy"])
    elif modelo[0] == 'A':
        #'A- validacao estratificada, rede 2 camadas (2048 1024_relu_dropout60 reduceLR shuffle metrica categorical_accuracy'
        model = Sequential()
        model.add(Dense(2048, input_shape=(qtd_colunas,), activation='relu'))
        model.add(Dropout(0.6))
        model.add(Dense(1024, activation='relu'))
        model.add(Dropout(0.6))
        model.add(Dense(treinamodelo['qtd_tipo'], activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=treinamodelo['nome_otimizador_modelo'],  metrics=["categorical_accuracy"])        
        #A- validacao estratificada, rede 3 camadas (1024_relu_dropout50_reduceLR shuffle metrica categorical_accuracy'
    elif modelo[0] == 'D':
        #"D- validacao estratificada, rede 2 camadas (2048_relu_dropout50 e 2048_sigmoid_dropout50) reduceLR50"
        model = Sequential()
        model.add(Dense(2048, input_shape=(qtd_colunas,), activation='relu'))
        model.add(Dropout(0.5))
        model.add(Dense(2048, activation='sigmoid'))
        model.add(Dropout(0.5))
        model.add(Dense(treinamodelo['qtd_tipo'], activation='softmax'))
        model.compile(loss='categorical_crossentropy', optimizer=treinamodelo['nome_otimizador_modelo'],  metrics=["categorical_accuracy"])        
        #D- validacao estratificada, rede 3 camadas (1024_relu_dropout50_reduceLR shuffle metrica categorical_accuracy'   
    else:
        raise Exception ('Não previsto modelo')
    """
elif detalhe_modelo in ['5 - validacao estratificada, rede BLSTM + 2 camadas (1024_relu_dropout40)']:
        input_layer = Input(shape=(treinamodelo['bag_num_palavras'],1))
        masking = Masking()(input_layer)
        x = Bidirectional(LSTM(treinamodelo['bag_num_palavras'], return_sequences=False))(masking)
        x = Dropout(0.4)(x)
        x = Dense(1024, activation="relu")(x)
        x = Dropout(0.4)(x)
        x = Dense(1024, activation="relu")(x)
        output = Dense(treinamodelo['qtd_tipo'], activation="softmax")(x)
        model = Model(inputs=input_layer, outputs=output)
        model.compile(loss='categorical_crossentropy', optimizer=treinamodelo['nome_otimizador_modelo'],  metrics=["accuracy"])
    """

    """
        input_layer = Input(shape=(256, 300))
        masking = Masking()(input_layer)
        x = Bidirectional(LSTM(256, return_sequences=False))(masking)
        x = Dropout(0.4)(x)
        x = Dense(1024, activation="relu")(x)
        x = Dropout(0.4)(x)
        x = Dense(1024, activation="relu")(x)
        output = Dense(NUM_CLASSES, activation="sigmoid")(x)
        model = Model(inputs=input_layer, outputs=output)
        model.compile(loss="binary_crossentropy", optimizer='adam', metrics=['accuracy'])
    """
    return model



tf.compat.v1.set_random_seed(cltreino.vrandom_state)   #tf.set_random_seed(vrandom_state)
lista_parametro_geracao_rede = ['bag_ind_modo_numerico', 'bag_num_palavras_conteudo',
       'bag_num_palavras_nome_arquivo', 'colunas_extras',
       'detalhe_classificador',
       'ind_tipo_numericalizacao', 'cod_metodo_extracao_texto',
       'nome_otimizador_modelo', 'num_epoca_maximo', 
       'num_tamanho_batch',  'qtd_dimensao_reduzida_conteudo', 
       'qtd_min_por_tipo', 'redutor_dimensao_conteudo',
       'se_treinamento_com_shuffle', 'descr_criterio_selecao']
