from rastro import treino_util as cltreino
from cladop import cladop_bd as cladopbd
from numericalizacao import trata_modelo as clu
from rastro import rastro_bd
import numpy as np
import timeit
from sklearn.metrics import accuracy_score
import datetime
from sklearn.model_selection import cross_validate
from sklearn.ensemble import RandomForestClassifier  # classificador default

# Author: marcusborela@yahoo.com.br
# antigo arquivo treina_classificador_bag_shallow.py     se necessário  busca de código antigo

def lista_faltando_parametro_gera_shallow(listas_grid_treinamento):
    listas_esperadas = ['lista_metodo', 'lista_criterio', 'lista_qtd_min_tipo', 'lista_modo_bag', 'lista_tamanho_bag_conteudo','lista_tamanho_bag_nome_arquivo',  'lista_detalhe_classificador', 'qtd_dimensao_reduzida_conteudo','redutor_dimensao_conteudo']
    listas_faltando = str([x for x in listas_esperadas if x not in listas_grid_treinamento ]) if len([x for x in listas_esperadas if x not in listas_grid_treinamento ]) > 1 else ''
    if ('kfold' in listas_grid_treinamento) or ('lista_percent_base_teste' in listas_grid_treinamento) or  ('lista_criterio_divisao_base_teste' in listas_grid_treinamento):
        listas_faltando += ' Erro: lista_criterio_divisao_bases lista_percent_base_teste e lista_criterio_divisao_base_teste não podem ser definidos na geração final do modelo'
    return str(listas_faltando)

def lista_faltando_grid_treinamento_shallow(listas_grid_treinamento):
    listas_esperadas = ['lista_metodo', 'lista_criterio', 'lista_qtd_min_tipo', 'lista_modo_bag', 'lista_tamanho_bag_conteudo','lista_tamanho_bag_nome_arquivo',  'lista_detalhe_classificador', 'qtd_dimensao_reduzida_conteudo','redutor_dimensao_conteudo']
    listas_faltando = str([x for x in listas_esperadas if x not in listas_grid_treinamento ]) if len([x for x in listas_esperadas if x not in listas_grid_treinamento ]) > 1 else ''
    if 'kfold' in listas_grid_treinamento and ('lista_percent_base_teste' in listas_grid_treinamento or  'lista_criterio_divisao_base_teste' in listas_grid_treinamento):
        listas_faltando += ' Erro: lista_criterio_divisao_bases lista_percent_base_teste não podem ser definidos ao mesmo tempo de kfold'
    if 'kfold' not in listas_grid_treinamento and 'lista_percent_base_teste' not in listas_grid_treinamento and  'lista_criterio_divisao_base_teste' not in listas_grid_treinamento:
        listas_faltando += ' Erro: kfold ou (lista_criterio_divisao_base_teste, lista_percent_base_teste) deve ser definido'
    return str(listas_faltando)

def treina_classificador_grid (listas_grid_treinamento, clf_tipo=RandomForestClassifier(bootstrap=False, n_estimators=40),num_iteracao_reinicio=1):
    # parâmetros não variáveis
    treinamodelo = {}
    #bases = {}
    treinamodelo['classificador']=str(type(clf_tipo))[8:-2]
    if lista_faltando_grid_treinamento_shallow(listas_grid_treinamento) != '':
        raise Exception (lista_faltando_grid_treinamento_shallow(listas_grid_treinamento))
    treinamodelo['ind_tipo_numericalizacao'] = 'Bag of words'  
    tot_iteracao = np.prod(np.array([len(x) for x in listas_grid_treinamento.values()]))
    print('tot_iteracao',tot_iteracao)
    cont_iteracao = 0
    for metodo in listas_grid_treinamento['lista_metodo']:
        print('Método *************************************************',metodo)
        treinamodelo['cod_metodo_extracao_texto']= metodo
        for criterio in listas_grid_treinamento['lista_criterio']:
            print('Critério *************************************************',criterio,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
            treinamodelo['descr_criterio_selecao']= criterio
            for qtd_min_por_tipo in listas_grid_treinamento['lista_qtd_min_tipo']:
                print('Qtd min tipo *************************************************',qtd_min_por_tipo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                treinamodelo['qtd_min_por_tipo']= qtd_min_por_tipo
                for colunas_extras in listas_grid_treinamento['colunas_extras']:
                    print('Colunas extras *************************************************',colunas_extras,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                    treinamodelo['colunas_extras'] = colunas_extras
                    bases = cladopbd.carrega_dados_df_y(treinamodelo['cod_metodo_extracao_texto'], treinamodelo['descr_criterio_selecao'],treinamodelo['qtd_min_por_tipo'],treinamodelo['colunas_extras'])
                    #df, y, bases['Labels'], bases['lbTipo']
                    treinamodelo['qtd_documento']= bases['df'].COD_DOCUMENTO.count()
                    treinamodelo['qtd_tipo']= len(bases['lbTipo'].classes_)
                    print('Qtd tipos:',treinamodelo['qtd_tipo'])
                    print('Qtd documentos:',treinamodelo['qtd_documento'])
                    for modo_bag in listas_grid_treinamento['lista_modo_bag']:
                        print('Modo bag *************************************************',modo_bag,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                        treinamodelo['bag_ind_modo_numerico'] = modo_bag
                        for tamanho_bag_conteudo in listas_grid_treinamento['lista_tamanho_bag_conteudo']:
                            print('Tamanho bag conteudo *************************************************',tamanho_bag_conteudo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                            treinamodelo['bag_num_palavras_conteudo'] = tamanho_bag_conteudo
                            X_antes_reduzir = clu.traduz_X_por_bag(np.array(bases['df'].TEXTO), treinamodelo['cod_metodo_extracao_texto'], treinamodelo['bag_num_palavras_conteudo'], treinamodelo['bag_ind_modo_numerico'])
                            for redutor_dim in listas_grid_treinamento['redutor_dimensao_conteudo']:
                                treinamodelo['redutor_dimensao_conteudo'] = redutor_dim
                                print('Redutor_dimensao_conteudo *************************************************',treinamodelo['redutor_dimensao_conteudo'],'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                for qtd_dim_reduzida in listas_grid_treinamento['qtd_dimensao_reduzida_conteudo']:
                                    treinamodelo['qtd_dimensao_reduzida_conteudo'] = qtd_dim_reduzida
                                    print('Qtd_dimensao_reduzida_conteudo *************************************************',treinamodelo['qtd_dimensao_reduzida_conteudo'])
                                    X_antes_nome = clu.trata_reducao_dimensao(X_antes_reduzir, treinamodelo['redutor_dimensao_conteudo'], treinamodelo['qtd_dimensao_reduzida_conteudo'])
                                    vcolunas_extras_texto=','.join(treinamodelo['colunas_extras']).strip()
                                    if vcolunas_extras_texto != '':  # tem colunas
                                        X_antes_nome = clu.adiciona_colunas_extras(X_antes_nome,treinamodelo['colunas_extras'], bases['df'])
                                    for tamanho_bag_nome_arquivo in listas_grid_treinamento['lista_tamanho_bag_nome_arquivo']:
                                        print('Tamanho bag nome_arquivo*************************************************',tamanho_bag_nome_arquivo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                        treinamodelo['bag_num_palavras_nome_arquivo'] = tamanho_bag_nome_arquivo
                                        bases['X'] = clu.traduz_nome_arquivo(X_antes_nome, bases['df'],treinamodelo['bag_num_palavras_nome_arquivo'], treinamodelo['bag_ind_modo_numerico'], treinamodelo['cod_metodo_extracao_nome_arquivo'])
                                        treinamodelo['qtd_colunas'] = bases['X'].shape[1] 
                                        if "kfold" in listas_grid_treinamento: # usar k-fold 
                                            for detalhe_classificador in listas_grid_treinamento['lista_detalhe_classificador']:
                                                clf = clf_tipo #volta ao default
                                                clf.set_params(**detalhe_classificador)
                                                treinamodelo['detalhe_classificador']= detalhe_classificador
                                                print(' Detalhe classificador *************************************************',detalhe_classificador)    
                                                for kf in  listas_grid_treinamento["kfold"]:       
                                                    treinamodelo['kfold'] =  kf
                                                    print('KF...', treinamodelo['kfold'])
                                                    cont_iteracao += 1
                                                    print('*** Iteracao', cont_iteracao, ' de ', tot_iteracao, ' em ', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                    if cont_iteracao >= num_iteracao_reinicio:
                                                        treina_classif_shallow_bag_kfold(bases['X'], bases['y_uma_coluna'], treinamodelo, clf)
                                                    else:
                                                        print('*** Saltando Iteracao')
                                        else:
                                            for percent_base_teste in listas_grid_treinamento['lista_percent_base_teste']:
                                                treinamodelo['percent_base_teste']= percent_base_teste
                                                print('Percent_base_teste *************************************************',percent_base_teste,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                for criterio_divisao_bases in listas_grid_treinamento['lista_criterio_divisao_base_teste']:
                                                    treinamodelo['criterio_divisao_base_teste']= criterio_divisao_bases
                                                    print('Criterio_divisao_bases *************************************************',criterio_divisao_bases,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                    bases['X_train'], bases['y_train'], bases['y_train_uma_coluna'], bases['X_test'], bases['y_test'], bases['y_test_uma_coluna']=cltreino.divide_2bases_quaisquer(bases['X'],bases['y'],treinamodelo['percent_base_teste'], treinamodelo['criterio_divisao_base_teste'])
                                                    for detalhe_classificador in listas_grid_treinamento['lista_detalhe_classificador']:
                                                        clf = clf_tipo #volta ao default
                                                        clf.set_params(**detalhe_classificador)
                                                        treinamodelo['detalhe_classificador']= detalhe_classificador
                                                        print(' Detalhe classificador *************************************************',detalhe_classificador,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")) 
                                                        cont_iteracao += 1
                                                        print('*** Iteracao', cont_iteracao, ' de ', tot_iteracao, ' em ', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                                        if cont_iteracao >= num_iteracao_reinicio:
                                                            treina_classif_shallow_bag(bases, treinamodelo, clf)     
                                                        else:
                                                            print('*** Saltando Iteracao')

    return treinamodelo, bases

def gera_classificador_shallow (listas_grid_treinamento, clf_tipo=RandomForestClassifier(bootstrap=False, n_estimators=40)):
    # parâmetros não variáveis
    treinamodelo = {}
    #bases = {}
    treinamodelo['classificador']=str(type(clf_tipo))[8:-2]
    if lista_faltando_parametro_gera_shallow(listas_grid_treinamento) != '':
        raise Exception (lista_faltando_parametro_gera_shallow(listas_grid_treinamento))
    treinamodelo['ind_tipo_numericalizacao'] = 'Bag of words'  
    tot_iteracao = np.prod(np.array([len(x) for x in listas_grid_treinamento.values()]))
    assert tot_iteracao == 1
    assert 'kfold' not in listas_grid_treinamento
    for metodo in listas_grid_treinamento['lista_metodo']:
        print('Método *************************************************',metodo)
        treinamodelo['cod_metodo_extracao_texto']= metodo
        for criterio in listas_grid_treinamento['lista_criterio']:
            print('Critério *************************************************',criterio,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
            treinamodelo['descr_criterio_selecao']= criterio
            for qtd_min_por_tipo in listas_grid_treinamento['lista_qtd_min_tipo']:
                print('Qtd min tipo *************************************************',qtd_min_por_tipo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                treinamodelo['qtd_min_por_tipo']= qtd_min_por_tipo
                for colunas_extras in listas_grid_treinamento['colunas_extras']:
                    print('Colunas extras *************************************************',colunas_extras,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                    treinamodelo['colunas_extras'] = colunas_extras
                    bases = cladopbd.carrega_dados_df_y(treinamodelo['cod_metodo_extracao_texto'], treinamodelo['descr_criterio_selecao'],treinamodelo['qtd_min_por_tipo'],treinamodelo['colunas_extras'])
                    #df, y, bases['Labels'], bases['lbTipo']
                    treinamodelo['qtd_documento']= bases['df'].COD_DOCUMENTO.count()
                    treinamodelo['qtd_tipo']= len(bases['lbTipo'].classes_)
                    print('Qtd tipos:',treinamodelo['qtd_tipo'])
                    print('Qtd documentos:',treinamodelo['qtd_documento'])
                    for modo_bag in listas_grid_treinamento['lista_modo_bag']:
                        print('Modo bag *************************************************',modo_bag,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                        treinamodelo['bag_ind_modo_numerico'] = modo_bag
                        for tamanho_bag_conteudo in listas_grid_treinamento['lista_tamanho_bag_conteudo']:
                            print('Tamanho bag conteudo *************************************************',tamanho_bag_conteudo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                            treinamodelo['bag_num_palavras_conteudo'] = tamanho_bag_conteudo
                            X_antes_reduzir = clu.traduz_X_por_bag(np.array(bases['df'].TEXTO), treinamodelo['cod_metodo_extracao_texto'], treinamodelo['bag_num_palavras_conteudo'], treinamodelo['bag_ind_modo_numerico'])
                            for redutor_dim in listas_grid_treinamento['redutor_dimensao_conteudo']:
                                treinamodelo['redutor_dimensao_conteudo'] = redutor_dim
                                print('Redutor_dimensao_conteudo *************************************************',treinamodelo['redutor_dimensao_conteudo'],'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                for qtd_dim_reduzida in listas_grid_treinamento['qtd_dimensao_reduzida_conteudo']:
                                    treinamodelo['qtd_dimensao_reduzida_conteudo'] = qtd_dim_reduzida
                                    print('Qtd_dimensao_reduzida_conteudo *************************************************',treinamodelo['qtd_dimensao_reduzida_conteudo'])
                                    X_antes_nome = clu.trata_reducao_dimensao(X_antes_reduzir, treinamodelo['redutor_dimensao_conteudo'], treinamodelo['qtd_dimensao_reduzida_conteudo'])
                                    vcolunas_extras_texto=','.join(treinamodelo['colunas_extras']).strip()
                                    if vcolunas_extras_texto != '':  # tem colunas
                                        X_antes_nome = clu.adiciona_colunas_extras(X_antes_nome,treinamodelo['colunas_extras'], bases['df'])
                                    for tamanho_bag_nome_arquivo in listas_grid_treinamento['lista_tamanho_bag_nome_arquivo']:
                                        print('Tamanho bag nome_arquivo*************************************************',tamanho_bag_nome_arquivo,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                        treinamodelo['bag_num_palavras_nome_arquivo'] = tamanho_bag_nome_arquivo
                                        bases['X'] = clu.traduz_nome_arquivo(X_antes_nome, bases['df'],treinamodelo['bag_num_palavras_nome_arquivo'], treinamodelo['bag_ind_modo_numerico'], treinamodelo['cod_metodo_extracao_nome_arquivo'])
                                        treinamodelo['qtd_colunas'] = bases['X'].shape[1]
                                        for detalhe_classificador in listas_grid_treinamento['lista_detalhe_classificador']:
                                            clf = clf_tipo #volta ao default
                                            clf.set_params(**detalhe_classificador)
                                            treinamodelo['detalhe_classificador']= detalhe_classificador
                                            print(' Detalhe classificador *************************************************',detalhe_classificador,'Dthora:', datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")) 
                                            treinamodelo['modelo_treinado']=treina_classif_shallow_bag_base_total(bases, treinamodelo, clf)     

    return treinamodelo, bases

def treina_classif_shallow_bag_kfold(X, y, treinamodelo, parm_classificador):
    """
    Recebe parâmetros de treinamento em treinamodelo
    E tipos existentes (cod, nome) em dfTipos
    E mod_palavras com o modelos de palavras (Tokenizer) gerado antes
    """
    lista_metrica = ["accuracy", "precision_macro", "precision_weighted", "f1_macro", "f1_weighted", "recall_macro", "recall_weighted" ]

    print('Fit modelo...')
    start = timeit.default_timer()
    vscore = cross_validate(parm_classificador, X, y, cv=treinamodelo['kfold'], n_jobs=-1, scoring=lista_metrica, return_train_score=True)
    stop = timeit.default_timer()
    treinamodelo['tempo_execucao'] = stop - start
    #print('tempo treinamento', treinamodelo['tempo_execucao'])
    # Apurando resultados
    for metrica in lista_metrica:
        if metrica != 'accuracy':
            nome="percent_"+metrica+"_teste"
            treinamodelo[nome]=vscore['test_'+ metrica].mean()
            print(nome, treinamodelo[nome])
        #print("Treinamento:", vscore['train_'+ metrica].mean())
    treinamodelo['percent_acuracia_teste'] = vscore['test_accuracy'].mean()
    treinamodelo['percent_acuracia_teste_std'] = vscore['test_accuracy'].std()
    treinamodelo['percent_acuracia_treina'] = vscore['train_accuracy'].mean()
    treinamodelo['percent_acuracia_treina_std'] = vscore['train_accuracy'].std()
    """
    treinamodelo['percent_acuracia_teste'] = vscore['test_score'].mean()
    treinamodelo['percent_acuracia_teste_std'] = vscore['test_score'].std()
    treinamodelo['percent_acuracia_treina'] = vscore['train_score'].mean()
    treinamodelo['percent_acuracia_treina_std'] = vscore['train_score'].std()
    """
    print('Acurácia alcançada em kfold - teste', treinamodelo['percent_acuracia_teste'], ' std de ', treinamodelo['percent_acuracia_teste_std'])    
    print('                        treinamento', treinamodelo['percent_acuracia_treina'], ' std de ', treinamodelo['percent_acuracia_treina_std'])    
    
    treinamodelo['cod_registro_treinamento'] = rastro.insere_resultado(treinamodelo)

    
    """
    if parm_se_gera_graficos:
        cnf_matrix_test = confusion_matrix(bases['y_test_uma_coluna'], y_pred_test)
        cnf_matrix_train = confusion_matrix(bases['y_train_uma_coluna'], y_pred_train)

        txt_passo=  'Calculando matriz de confusão e plotando resultados'
        print(txt_passo)

        print('Avaliação do modelo com dados de treinamento - por tipo')
        plot_confusion_matrix(cnf_matrix_train,classes=bases['Labels'], cmap='YlGn', normalize=False)
        print(classification_report(bases['y_train_uma_coluna'],y_pred_train,digits=5))   

        print('Avaliação do modelo com dados de teste - por tipo')
        plot_confusion_matrix(cnf_matrix_test,classes=bases['Labels'], cmap='YlGn', normalize=False)
        print(classification_report(bases['y_test_uma_coluna'],y_pred_test,digits=5))    
    """

    return

def gera_listas_grid_treinamento_shallow(parm_tipo='sem_Kfold'): 
    listas_grid_treinamento = cltreino.gera_listas_grid_treinamento(parm_tipo)
    listas_grid_treinamento['lista_detalhe_classificador'] = ['bootstrap=False, n_estimators=40']
    if parm_tipo=='sem_kfold':
        listas_grid_treinamento['lista_criterio_divisao_bases'] = ['base teste nao estratificada, base teste sem shuffle']        
    return listas_grid_treinamento

def gera_parametros_classificador(classificador, parm_variacao):
    import itertools

    parm_lista = list(parm_variacao.items())
    lista_combinacoes=[]
    qtd=0
    totitens = len(parm_variacao)
    lista_valores = list(parm_lista[i][1] for i in range(totitens))
    lista_labels = list(parm_lista[i][0] for i in range(totitens))
    valores_permutados = list(itertools.product(*lista_valores))
    #print(valores_permutados)
    #print(lista_labels)
    parametro_clf_base = classificador.get_params(deep=True)
    for combinacao in valores_permutados:
        parametro_clf = parametro_clf_base.copy()
        #print(combinacao)
        for ind_parm in range(totitens):
            #print(lista_labels[ind_parm], combinacao[ind_parm] ) 
            parametro_clf[lista_labels[ind_parm]] = combinacao[ind_parm]
        #print(parametro_clf_base)
        lista_combinacoes.append(parametro_clf)
        qtd+=1
    print('Combinados ',totitens, ' parâmetros', ' em ', qtd, ' combinaçoes')
    return lista_combinacoes   

def treina_classif_shallow_bag_base_total(bases, treinamodelo, parm_classificador):
    """
    Recebe parâmetros de treinamento em treinamodelo
    E tipos existentes (cod, nome) em dfTipos
    E mod_palavras com o modelos de palavras (Tokenizer) gerado antes
    """
    start = timeit.default_timer()
    #callbacks.EarlyStopping(monitor='loss', patience=4, verbose=1, mode='min'),
    
    txt_passo= 'Fit modelo...'
    print(txt_passo)
    parm_classificador.fit(bases['X'],bases['y_uma_coluna'])
    stop = timeit.default_timer()
    
    treinamodelo['tempo_execucao'] = stop - start
    
    print('tempo treinamento', treinamodelo['tempo_execucao'])
    
    # Apurando resultados
    txt_passo= 'Apurando resultados...'
    print(txt_passo)

    y_pred_train = parm_classificador.predict(bases['X'])
    treinamodelo['percent_acuracia_treina'] = accuracy_score(bases['y_uma_coluna'],y_pred_train)

    
    
    print('Acurácia alcançada em treinamento:', treinamodelo['percent_acuracia_treina'])
    # salva resultado no banco de dados
    treinamodelo['cod_registro_treinamento'] = rastro.insere_resultado(treinamodelo)
    return parm_classificador

def treina_classif_shallow_bag(bases, treinamodelo, parm_classificador):
    """
    Recebe parâmetros de treinamento em treinamodelo
    E tipos existentes (cod, nome) em dfTipos
    E mod_palavras com o modelos de palavras (Tokenizer) gerado antes
    """
    start = timeit.default_timer()
    #callbacks.EarlyStopping(monitor='loss', patience=4, verbose=1, mode='min'),
    
    txt_passo= 'Fit modelo...'
    print(txt_passo)
    parm_classificador.fit(bases['X_train'],bases['y_train_uma_coluna'])
    stop = timeit.default_timer()
    
    treinamodelo['tempo_execucao'] = stop - start
    
    print('tempo treinamento', treinamodelo['tempo_execucao'])
    
    # Apurando resultados
    txt_passo= 'Apurando resultados...'
    print(txt_passo)

    y_pred_train = parm_classificador.predict(bases['X_train'])
    treinamodelo['percent_acuracia_treina'] = accuracy_score(bases['y_train_uma_coluna'],y_pred_train)

    y_pred_test = parm_classificador.predict(bases['X_test'])
    treinamodelo['percent_acuracia_teste'] = accuracy_score(bases['y_test_uma_coluna'],y_pred_test)
    
    
    print('Acurácia alcançada em treinamento:', treinamodelo['percent_acuracia_treina'])
    print('Acurácia alcançada em teste:', treinamodelo['percent_acuracia_teste'])

    # salva resultado no banco de dados
    treinamodelo['cod_registro_treinamento'] = rastro.insere_resultado(treinamodelo)

    return 
    


