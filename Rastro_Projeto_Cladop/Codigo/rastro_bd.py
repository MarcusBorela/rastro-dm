import re
import datetime
from util import util_bd as utilbd
from unicodedata import normalize
# para as rotinas de carregar parâmetros a partir de execuções, é necessário ter os tipos definidos para otimizadores e redutores de demensão
from keras.optimizers import  RMSprop, Adam, Adadelta
from sklearn.decomposition import TruncatedSVD
from sklearn.model_selection import ShuffleSplit, RepeatedKFold, KFold # usado ao gerar lista

# Author: marcusborela@yahoo.com.br

def traduz_redutor(parm_nome_redutor_padrao):
    return eval(re.sub('\n', '', parm_nome_redutor_padrao))

def traduz_nome_otimizador(parm_nome_otimizador_padrao):
    """
    Recebe nome otimizador salvo em banco de  dados no formato 
     "<class 'keras.optimizers.Adam'> Parâmetros: {'lr': 9.999999747378752e-05, 'beta_1': 0.8999999761581421, 'beta_2': 0.9990000128746033, 'decay': 0.0, 'epsilon': 1e-07, 'amsgrad': False}"
    gerado como 
      str(type(treinamodelo['nome_otimizador_modelo'])) + ' Parâmetros: ' + str(treinamodelo['nome_otimizador_modelo'].get_config())
    E retorna um otimizador propriamente dito
    """    
    nome_otimizador = re.findall("keras\.optimizers\.(.*)\'>",parm_nome_otimizador_padrao)[0]
    lista_parametro = re.findall("Parâmetros\: \{(.*)\}",parm_nome_otimizador_padrao)[0] 
    lista_parametro = re.sub("'","", lista_parametro)
    lista_parametro = re.sub(":","=", lista_parametro)
    return eval(nome_otimizador + "("+ lista_parametro + ")")
   

def carregar_parametro_execucao(parm_cod_execucao):
    """
    Carrega parâmetros não nulos usados em uma execução de treinamento
    retorna dicionário treinamodelo com os valores
    """
    treinamodelo = {}
    query = """
    select 
    NOME_MODELO_CLASSIFICADOR as classificador,
    DESCR_CRITERIO_DIV_BASE_VALIDA as criterio_divisao_base_valid,
    NUM_EPOCA_MAXIMO,
    NUM_TAMANHO_BATCH,
    IND_TIPO_NUMERICALIZACAO,
    COD_METODO_EXTRACAO_TEXTO,
    cod_metodo_extracao_nome_arq,
    descr_criterio_selecao,
    kfold,
    QTD_MIN_POR_TIPO,
    DESCR_COLUNA_EXTRA as colunas_extras,
    BAG_IND_MODO_NUMERICO,
    BAG_NUM_PALAVRAS_CONTEUDO,
    REDUTOR_DIMENSAO_CONTEUDO,  -- pegar valor da variável tirar \n
    QTD_DIMENSAO_REDUZIDA_CONTEUDO,
    PERCENT_BASE_VALIDACAO,
    SE_TREINAMENTO_COM_SHUFFLE, -- precisa converter para boolean
    NOME_OTIMIZADOR_MODELO, 
    BAG_NUM_PALAVRAS_NOME_ARQUIVO,
    DESCR_DETALHE_CLASSIFICADOR detalhe_classificador,
    PERCENT_BASE_TESTE, -- pode não ter
    NUM_EPOCA_TREINA,  -- pode não ter, pode ser parâmetro se não houver dados de validação
    DESCR_CRITERIO_DIV_BASE_TESTE as criterio_divisao_base_teste -- pode não ter
    from 
       TREINAMENTO_CLASSIFICADOR t
    where
    t.cod={} """.format(parm_cod_execucao)
    df = utilbd.retorna_df_oracle(query)
    treinamodelo = {}
    for nomecol in df.columns:
        if nomecol == "COLUNAS_EXTRAS" or df[[nomecol]].iloc[0][0] is not None:
            treinamodelo[nomecol.lower()]=df[[nomecol]].iloc[0][0]
    treinamodelo['se_treinamento_com_shuffle']=treinamodelo['se_treinamento_com_shuffle']=='S'
    treinamodelo['redutor_dimensao_conteudo'] = traduz_redutor(treinamodelo['redutor_dimensao_conteudo'])
    if treinamodelo['colunas_extras'] is None:
        treinamodelo['colunas_extras'] = ""
    if treinamodelo['kfold'] is not None:        
        treinamodelo['kfold'] = eval(treinamodelo['kfold'])
    treinamodelo['nome_otimizador_modelo']= traduz_nome_otimizador(treinamodelo['nome_otimizador_modelo'])
    treinamodelo['cod_metodo_extracao_nome_arquivo'] = treinamodelo['cod_metodo_extracao_nome_arq']
    del treinamodelo['cod_metodo_extracao_nome_arq']
    return treinamodelo

def retorna_proximo_cod_registro_treinamento():
    query = """
        select 
          TREINAMENTO_CLASSIFICADOR_SEQ.nextval
        from 
        dual"""
    df= utilbd.retorna_df_oracle(query)
    return df.iloc[0][0]

def retorna_texto_kfold(kf):
    # RepeatedKFold não retorna um texto adequado para salvar no bd, logo precisamos formatar antes
    # shuffleSplit e KFold retornam
    if 'RepeatedKFold' in str(type(kf)):
        return str(type(kf))[39:-2] + '(n_splits=' + str(kf.get_n_splits()/kf.n_repeats) + ', n_repeats = ' + str(kf.n_repeats)+ ', random_state = ' + str(kf.random_state) + ')'
    else: 
        return str(kf)

def insere_resultado(treinamodelo):
    """
    """     
    global vcontexto
    

    # calcula colunas redundantes
    agora = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")        

    # tratar colunas que podem faltar
    vtempo_execucao = str(round(treinamodelo['tempo_execucao'],10)) if 'tempo_execucao' in treinamodelo  else 'null'
    vpercent_base_teste = str(round(treinamodelo['percent_base_teste'],3)) if 'percent_base_teste' in treinamodelo  else 'null'
    vpercent_acuracia_teste = str(round(treinamodelo['percent_acuracia_teste'],3)) if 'percent_acuracia_teste' in treinamodelo  else 'null'
    vpercent_acuracia_treina = str(round(treinamodelo['percent_acuracia_treina'],3)) if 'percent_acuracia_treina' in treinamodelo  else 'null'
    vcriterio_divisao_base_teste = treinamodelo['criterio_divisao_base_teste'] if 'criterio_divisao_base_teste' in treinamodelo  else ''
    vcriterio_divisao_base_valid = treinamodelo['criterio_divisao_base_valid'] if 'criterio_divisao_base_valid' in treinamodelo  else ''
    vkfold = retorna_texto_kfold(treinamodelo['kfold']) if 'kfold' in treinamodelo  else '' 
    vpercent_acuracia_treina_std =  str(round(treinamodelo['percent_acuracia_treina_std'],3)) if 'percent_acuracia_treina_std' in treinamodelo  else 'null' 
    vpercent_acuracia_valida_std =  str(round(treinamodelo['percent_acuracia_valida_std'],3)) if 'percent_acuracia_valida_std' in treinamodelo  else 'null' 
    vpercent_acuracia_teste_std =  str(round(treinamodelo['percent_acuracia_teste_std'],3)) if 'percent_acuracia_teste_std' in treinamodelo  else 'null' 
    vpercent_acuracia_valida = str(round(treinamodelo['percent_acuracia_valida'],3)) if 'percent_acuracia_valida' in treinamodelo else 'null' 
    vpercent_base_validacao = str(round(treinamodelo['percent_base_validacao'],3))  if 'percent_base_validacao' in treinamodelo else 'null' 
    vnome_otimizador_modelo = str(type(treinamodelo['nome_otimizador_modelo'])) + ' Parâmetros: ' + str(treinamodelo['nome_otimizador_modelo'].get_config()) if 'nome_otimizador_modelo' in treinamodelo  else ''     
    vnum_epoca_maximo = str(treinamodelo['num_epoca_maximo']) if 'num_epoca_maximo' in treinamodelo else 'null' 
    vnum_tamanho_batch = str(treinamodelo['num_tamanho_batch']) if 'num_tamanho_batch' in treinamodelo else 'null' 
    vnum_epoca_treina  = str(treinamodelo['num_epoca_treina']) if 'num_epoca_treina' in treinamodelo else 'null' 
    vcolunas_extras="'" + re.sub("'", '"',','.join(treinamodelo['colunas_extras']).strip())[0:3999] +  "'" if 'colunas_extras' in treinamodelo else 'null'
    vredutor_dimensao_conteudo = re.sub("'", '"',str(treinamodelo['redutor_dimensao_conteudo'])) if (treinamodelo['redutor_dimensao_conteudo'] != '' and treinamodelo['qtd_dimensao_reduzida_conteudo'] != '') else ''
    vqtd_dimensao_reduzida_conteudo = str(treinamodelo['qtd_dimensao_reduzida_conteudo']) if (treinamodelo['redutor_dimensao_conteudo'] != '' and treinamodelo['qtd_dimensao_reduzida_conteudo'] != '') else ''
    vpercent_precision_macro_teste =  str(round(treinamodelo['percent_precision_macro_teste'],3)) if 'percent_precision_macro_teste' in treinamodelo  else 'null' 
    vpercent_precision_weighted_teste =  str(round(treinamodelo['percent_precision_weighted_teste'],3)) if 'percent_precision_weighted_teste' in treinamodelo  else 'null' 
    vpercent_f1_macro_teste =  str(round(treinamodelo['percent_f1_macro_teste'],3)) if 'percent_f1_macro_teste' in treinamodelo  else 'null' 
    vpercent_f1_weighted_teste =  str(round(treinamodelo['percent_f1_weighted_teste'],3)) if 'percent_f1_weighted_teste' in treinamodelo  else 'null' 
    vpercent_recall_macro_teste =  str(round(treinamodelo['percent_recall_macro_teste'],3)) if 'percent_recall_macro_teste' in treinamodelo  else 'null' 
    vpercent_recall_weighted_teste =  str(round(treinamodelo['percent_recall_weighted_teste'],3)) if 'percent_recall_weighted_teste' in treinamodelo  else 'null' 
    vnome_modelo_classificador = treinamodelo['classificador'][0:254]
    vnome_modelo_classificador = vnome_modelo_classificador.rsplit('.', 1)[-1] if '.' in vnome_modelo_classificador else vnome_modelo_classificador
    if 'se_treinamento_com_shuffle' in treinamodelo:
        if treinamodelo['se_treinamento_com_shuffle']:
            vse_treinamento_com_shuffle = "'" +  "S" + "'" 
        else:
            vse_treinamento_com_shuffle = "'" +  "N" + "'" 
    else:
        vse_treinamento_com_shuffle = 'null'
    cod_registro_treinamento_insercao=retorna_proximo_cod_registro_treinamento()
    comando  = """INSERT INTO TREINAMENTO_CLASSIFICADOR (
        cod,
        cod_projeto,  -- assumido 1, cladop
        dthora_registro,     
        nome_modelo_classificador,
        descr_criterio_selecao,  
        cod_metodo_extracao_texto,     
        cod_metodo_extracao_nome_arq,     
        bag_num_palavras_conteudo,        
        bag_num_palavras_nome_arquivo,        
        bag_ind_modo_numerico,   
        qtd_documento,           
        qtd_tipo,                
        num_epoca_maximo,        
        num_tamanho_batch,       
        percent_base_validacao,  
        percent_base_teste,      
        percent_acuracia_teste,  
        percent_acuracia_treina, 
        descr_detalhe_classificador,   
        descr_criterio_div_base_teste,
        descr_criterio_div_base_valida,
        nome_otimizador_modelo,
        percent_acuracia_valida,
        num_epoca_treina,
        qtd_min_por_tipo,
        tempo_execucao,
        ind_tipo_numericalizacao,
        descr_coluna_extra, 
        redutor_dimensao_conteudo,
        qtd_dimensao_reduzida_conteudo,
        kfold,
        percent_acuracia_treina_std,
        percent_acuracia_teste_std,
        percent_acuracia_valida_std,
        percent_precision_macro_teste,
        percent_precis_weighted_teste,
        percent_f1_macro_teste,
        percent_f1_weighted_teste,
        percent_recall_macro_teste,
        percent_recall_weighted_teste,
        se_treinamento_com_shuffle,
        descr_contexto
        )  
    VALUES ("""+ str(cod_registro_treinamento_insercao)+ ", " + str(vcod_projeto) + ", to_date('" \
    + agora + "', 'dd/mm/yyyy hh24:mi:ss') " \
        + ", " + "'" + vnome_modelo_classificador + "'"   \
        + ", " + "'" + str(treinamodelo['descr_criterio_selecao']).replace("'", "''")[0:3999] + "'"   \
        + ", " + str(treinamodelo['cod_metodo_extracao_texto'])   \
        + ", " + str(treinamodelo['cod_metodo_extracao_nome_arquivo'])   \
        + ', ' + str(treinamodelo['bag_num_palavras_conteudo'])   \
        + ', ' + str(treinamodelo['bag_num_palavras_nome_arquivo'])   \
        + ", " + "'" + str(treinamodelo['bag_ind_modo_numerico'])+ "'"      \
        + ', ' + str(treinamodelo['qtd_documento'])   \
        + ', ' + str(treinamodelo['qtd_tipo'])        \
        + ', ' + vnum_epoca_maximo \
        + ', ' + vnum_tamanho_batch \
        + ', ' + vpercent_base_validacao \
        + ', ' + vpercent_base_teste \
        + ', ' + vpercent_acuracia_teste \
        + ', ' + vpercent_acuracia_treina \
        + ", " + "'" + str(treinamodelo['detalhe_classificador']).replace("'", "''")[-3999:] + "'"  \
        + ", " + "'" + vcriterio_divisao_base_teste[0:299] + "'"  \
        + ", " + "'" + vcriterio_divisao_base_valid[0:299] + "'"  \
        + ", " + "'" + vnome_otimizador_modelo.replace("'", "''")[0:499] + "'"  \
        + ', ' + vpercent_acuracia_valida \
        + ', ' + vnum_epoca_treina \
        + ', ' + str(treinamodelo['qtd_min_por_tipo']) \
        + ', ' +  vtempo_execucao \
        + ', ' + "'" + str(treinamodelo['ind_tipo_numericalizacao'])[0:99] + "'" \
        + ', ' +  vcolunas_extras \
        + ', ' + "'" +  vredutor_dimensao_conteudo[0:999]  + "'"  \
        + ', ' + "'" +  vqtd_dimensao_reduzida_conteudo + "'"   \
        + ', ' + "'" + vkfold[0:399]  + "'" \
        + ', ' + vpercent_acuracia_treina_std \
        + ', ' + vpercent_acuracia_teste_std \
        + ', ' + vpercent_acuracia_valida_std \
        + ', ' + vpercent_precision_macro_teste \
        + ', ' + vpercent_precision_weighted_teste \
        + ', ' + vpercent_f1_macro_teste \
        + ', ' + vpercent_f1_weighted_teste \
        + ', ' + vpercent_recall_macro_teste \
        + ', ' + vpercent_recall_weighted_teste \
        + ', ' + vse_treinamento_com_shuffle   \
        + ', ' + "'" +  vcontexto[0:999] + "'" + ')'

    #print('Comando: ',comando)
    utilbd.executa_insert_oracle(comando,auto_commit=True)
    return cod_registro_treinamento_insercao

def set_cod_projeto(parm_cod_projeto=1):
    """
        Permite definir cod_projeto a ser usado pelas rotinas
    """       
    global vcod_projeto
    vcod_projeto = parm_cod_projeto
    return

def get_cod_projeto():
    global vcod_projeto
    return vcod_projeto



def remover_acentos(string):
    #  normalize("NFKD", string)  equivale a separar ç colocando c e cedilha
    #  encode('ascii', errors='ignore') - cortando o cedilha
    return normalize("NFKD", string).encode('ascii', errors='ignore').decode('utf-8')

def registrar_observacao_projeto(parm_texto, parm_contexto=' '):
    """
        Registra definição de ação e aprendizado do projeto; a ser categorizado a posterior
        
        Comandos para definir contexto no notebook
        %%javascript
        IPython.notebook.kernel.execute('notebook_name = "' + IPython.notebook.notebook_name + '"')
    """     
    global vcontexto
    if parm_contexto==' ':
        parm_contexto= vcontexto
    qtd_inserido = 0
    # tratando acento
    vtexto=remover_acentos(parm_texto)
    vtexto =re.sub("'", " ",vtexto)
    # calcula colunas redundantes
    agora = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")        
    comando  = """INSERT INTO observacao_projeto (cod_projeto, 
        dthora_registro, descr, descr_contexto)
    VALUES (""" + str(vcod_projeto) + ", " + " to_date('" \
    + agora + "', 'dd/mm/yyyy hh24:mi:ss'), " + "'" + vtexto + "', '" + parm_contexto + "')"   
    qtd_inserido = utilbd.executa_insert_oracle(comando,auto_commit=True)
    return qtd_inserido 

def set_contexto_log(parm_contexto):
    global vcontexto
    vcontexto = parm_contexto
    return

def get_contexto_log():
    global vcontexto
    return vcontexto

vcontexto = ''
vcod_projeto=1 # Cladop
