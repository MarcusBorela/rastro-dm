import numpy as np
from os.path import expanduser
from sklearn.model_selection import train_test_split
from sklearn.model_selection import ShuffleSplit #  RepeatedKFold  # usado ao gerar lista
from sklearn.decomposition import TruncatedSVD  # usado ao gerar lista

# Author: marcusborela@yahoo.com.br
# originado de partes de antigos arquivos util.py e util_bd.py    se necessário  busca de código antigo
 
def gera_listas_grid_treinamento(parm_tipo='sem_Kfold'): 

    """
    Outros valores possíveis
    listas_grid_treinamento["kfold"] 
      KFold(n_splits=10,shuffle = True, random_state = clu.vrandom_state)
    , KFold(n_splits=10,shuffle = False, random_state = clu.vrandom_state)
    , KFold(n_splits=5,shuffle = True, random_state = clu.vrandom_state)
    , KFold(n_splits=5,shuffle = False, random_state = clu.vrandom_state)
    , StratifiedKFold(n_splits=10,shuffle = True, random_state = clu.vrandom_state)
    , StratifiedKFold(n_splits=10,shuffle = False, random_state = clu.vrandom_state)
    , StratifiedKFold(n_splits=5,shuffle = True, random_state = clu.vrandom_state)
    , StratifiedKFold(n_splits=5,shuffle = False, random_state = clu.vrandom_state)
    , ShuffleSplit(n_splits=10, test_size=0.15, random_state=clu.vrandom_state)
    , ShuffleSplit(n_splits=5, test_size=0.15, random_state=clu.vrandom_state)
    , StratifiedShuffleSplit(n_splits=10, test_size=0.15, random_state=clu.vrandom_state)
    , StratifiedShuffleSplit(n_splits=5, test_size=0.15, random_state=clu.vrandom_state)
    """
    global vrandom_state, vcod_projeto
    if vcod_projeto != 1:
        raise Exception('Não previsto para código de projeto')
    else:
        listas_grid_treinamento = {}
            
        listas_grid_treinamento['lista_modo_bag'] = ['binary', 'tfidf' ]
        # retirados pois não são bons , 'count', 'freq'
        if parm_tipo=='sem_kfold':
            listas_grid_treinamento['lista_percent_base_teste'] = [0.15]    
        elif parm_tipo=='kfold':
            listas_grid_treinamento["kfold"] = [ShuffleSplit(n_splits=3,test_size=0.15, random_state = vrandom_state)]
            #listas_grid_treinamento["kfold"] = [KFold(n_splits=7,shuffle = True, random_state = vrandom_state)]

        listas_grid_treinamento['lista_tamanho_bag_conteudo'] = [8192]
        listas_grid_treinamento['lista_tamanho_bag_nome_arquivo'] = [512]
        listas_grid_treinamento['redutor_dimensao_conteudo'] = [TruncatedSVD(random_state=vrandom_state, algorithm='arpack', n_components=43)]
        listas_grid_treinamento['qtd_dimensao_reduzida_conteudo'] = [43]


        listas_grid_treinamento['lista_metodo'] =  [6]       

        listas_grid_treinamento['lista_detalhe_classificador'] = [{'bootstrap':False, 'n_estimators':40}]  

        listas_grid_treinamento['lista_criterio'] =   [
    'doc.cod_tipo <> 41 -- outros',
    'doc.cod_tipo <> 41 and ((AVA.qtd_token_palavra_valida/ava.qtd_pagina_pdf) >= 30) '
    ]  

        listas_grid_treinamento['lista_qtd_min_tipo'] =   [10]       

    return listas_grid_treinamento

def set_random_state(parm_random_state):
    global vrandom_state
    if parm_random_state:
        vrandom_state = parm_random_state
    return vrandom_state

def get_random_state():
    global vrandom_state
    return vrandom_state

def divide_2bases_quaisquer(X, y, parm_percent_base, parm_criterio):
    # divide em 2 bases dado criterio 
    # se parm_criterio nao estabelece com shuffle nem nao estratificada assumido
    # shuffle sim e estrataificada sim
    # If shuffle=False then stratify must be None.
    if 'com estratificacao' not in parm_criterio.lower() and 'sem estratificacao' not in parm_criterio.lower():
        raise Exception('divide_2bases_quaisquer - nao definido criterio de estratificacao em ' + parm_criterio)
    if 'com shuffle' not in parm_criterio.lower() and 'sem shuffle' not in parm_criterio.lower():
        raise Exception('divide_2bases_quaisquer - nao definido criterio de shuffle em ' + parm_criterio)
    vcom_shuffle = 'com shuffle' in parm_criterio.lower()
    vestratificada = 'com estratificacao' in parm_criterio.lower()
    if not vcom_shuffle and vestratificada:
        raise Exception('divide_2bases_quaisquer - criterio nao pode ser sem shuffle e estratificada')
    #print('variavel vestratificada=', str(vestratificada),'parm_criterio.lower()',parm_criterio.lower())
    if vestratificada:
        X1, X2, y1, y2 = train_test_split(X, y, test_size=parm_percent_base, random_state=vrandom_state, shuffle=vcom_shuffle, stratify=y)
    else:
        X1, X2, y1, y2 = train_test_split(X, y, test_size=parm_percent_base, random_state=vrandom_state, shuffle=vcom_shuffle,stratify=None)
    y1_uma_coluna = [np.argmax(t) for t in y1]
    y2_uma_coluna = [np.argmax(t) for t in y2]
    return X1, y1,y1_uma_coluna, X2, y2, y2_uma_coluna

def set_path_modelo(parm_path_modelo=1):
    """
        Permite definir cod_projeto a ser usado pelas rotinas
    """       
    global vpath_modelo
    vpath_modelo = parm_path_modelo
    return

def get_path_modelo():
    global vpath_modelo
    return vpath_modelo


def imprime_dicionario(parm_dict):
    for k, v in parm_dict.items():
        print (k, "-->", v)

def imprime_treinamodelo(parm_dict):
    for k, v in parm_dict.items():
        if k not in []:
            print (k, "-->", v)

def imprime_dicionario_chaves(parm_dict):
    for k, v in parm_dict.items():
        print (k)

vrandom_state=42
np.random.seed(vrandom_state)
vpath_modelo = expanduser("~") +'/etce/modelos/'
delo = expanduser("~") +'/etce/modelos/'
